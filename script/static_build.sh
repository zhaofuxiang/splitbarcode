
#export LD_LIBRARY_PATH=/ldfssz1/ST_BI/bcbuild/01.lib/boost_1_60_0/lib:/ldfssz1/ST_BI/bcbuild/01.lib/zlib-1.2.8/lib
export LIBRARY_PATH=/ldfssz1/ST_BI/bcbuild/01.lib/boost_1_60_0/lib:/ldfssz1/ST_BI/bcbuild/01.lib/zlib-1.2.8/lib:/usr/lib/x86_64-redhat-linux6E/lib64/

export CPLUS_INCLUDE_PATH=/ldfssz1/ST_BI/bcbuild/01.lib/boost_1_60_0/include:/ldfssz1/ST_BI/bcbuild/01.lib/zlib-1.2.8/include

mkdir -p bin

binPath=/ldfssz1/ST_BI/bcbuild/01.lib/gcc-6.1.0/bin
#/usr/bin/g++ -static -std=c++11 *.cpp -o splitBarcode  -lboost_system -lboost_chrono -lboost_thread -lboost_filesystem -lboost_iostreams -pthread -lz 
$binPath/g++ -static -O2 -std=c++11 splitBarcode/*.cpp -o bin/splitBarcode  -lboost_system -lboost_chrono -lboost_thread -lboost_filesystem -lboost_iostreams -pthread -lz -lrt -Dlinux
