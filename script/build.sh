

libPath="/ldfssz1/ST_BI/bcbuild/01.lib"
gccPath="$libPath/gcc-6.1.0"
zlibPath="$libPath/zlib-1.2.8"
boostPath="$libPath/boost_1_60_0"

otherPath="/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin"
binPath="/ldfssz1/ST_BI/bcbuild/02.bin"

export PATH="$binPath:$otherPath"

export LD_LIBRARY_PATH="$gccPath/lib64:$zlibPath/lib:$boostPath/lib"
export LIBRARY_PATH=$LD_LIBRARY_PATH

export C_INCLUDE_PATH="$gccPath/include:$zlibPath/include:$boostPath/include"
export CPLUS_INCLUDE_PATH=$C_INCLUDE_PATH

export CC="$gccPath/bin/gcc"
export CXX="$gccPath/bin/g++"

srcPath="$(pwd)"/splitBarcode
installPath="$(pwd)"

buildPath="$(pwd)/build"
if [ -e "buildPath" ]
then 
    rm -rf $buildPath
fi
mkdir -p $buildPath

cd $buildPath



cmake -DCMAKE_INSTALL_PREFIX="$installPath" $srcPath
make 

if [ -e "$buildPath" ]
then 
    rm -rf $buildPath
fi
