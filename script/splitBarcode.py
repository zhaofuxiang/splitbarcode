#!/usr/bin/env python3

### Copyright 2018 BGI-Research, Shenzhen. All Rights Reserved.
### Confidential and proprietary works of BGI-Research.
###### Import Modules
import sys
import os
from copy import copy
from itertools import combinations_with_replacement, product, chain
import gzip

###### Document Decription
"""  """

###### Version and Date
PROG_VERSION = '0.1.0'
PROG_DATE = '2018-08-20'

###### Usage
USAGE = """

     Version %s  by Vincent Li  %s

     Usage: %s <barcode> <fq> >STDOUT
""" % (PROG_VERSION, PROG_DATE, os.path.basename(sys.argv[0]))

######## Global Variable


#######################################################################
############################  BEGIN Class  ############################
#######################################################################
class Barcode(object):
    """ docstring of Barcode """
    bases = ("A", "C", "G", "T", "N")

    def __init__(self, barcodeFile=None):
        self.lens = 0
        self.count = 0
        self.misDict = {}
        self.transTable = str.maketrans('ACGT', 'TGCA')

        if barcodeFile:
            self.load(barcodeFile)

    def load(self, f):
        """
            Load barcode sequence from file
            Attributes:
                barcode:
                    key: barcode ID
                    value: barcode sequence
                lens: Barcode length
                count: Barcode number
                misDict:
                    key: barcode sequence with mismatch
                    value: barcode ID
        """
        self.barcode = {}
        self.lens = 0
        self.count = 0
        with open(f, 'r', 10000000) as fh:
            for line in fh:
                info = line.split()
                if len(info) < 2:
                    continue
                self.barcode[info[0]] = info[1]
                if self.lens and self.lens != len(info[1]):
                    raise RuntimeError("[ERROR] Got different barcode length, last: {:d}, {:s}: {:d}".format(self.lens, info[1], len(info[1])))
                self.lens = len(info[1])
                self.count += 1
        return self.barcode

    def reverseComplement(self):
        """
            Reverse complement of barcode sequence.
            Directly change self.barcode
        """
        for b in self.barcode:
            self.barcode[b] = self.barcode[b][::-1].translate(self.transTable)
        ## clear mismatch dict
        self.misDict = {}
        return self.barcode

    def _getPosIdx(self, length, error, start=0):
        """
            Use combination to pick error position and create a list of position index.
            Return:
                list of position index:
                example:
                    [{0}, {1, 2}]
                elements:
                    set of positions that needed to be changed.
        """
        return [set(x) for x in combinations_with_replacement(range(start, start + length), error)]

    def _getMisPosArr(self, misArr):
        """
            Use product to create a list of multiple subindex.
            subIndex1:
                [{0}, {1, 2}]
            subIndex2:
                [{3}, {4, 5}]
            Product:
                [(0, 3), (0, 4, 5), (1, 2, 3), (1, 2, 4, 5)]
        """
        st = 0
        misPosArr = []
        for k in misArr:
            posIdx = self._getPosIdx(k[0], k[1], start=st)
            st += k[0]
            misPosArr.append(posIdx)
        return [tuple(chain(*x)) for x in product(*misPosArr)]

    def createMis(self, idx, seq, misPosArr):
        """
            Create mismatch sequence and add to mismatch dict.
        """
        if seq in self.misDict:
            self.misDict[seq] = "ambiguous"
        else:
            self.misDict[seq] = idx

        ## loop all posIndex condictions
        for posArr in misPosArr:
            seqlist = list(seq)

            posCnt = len(posArr)
            # varInter = product(self.bases, repeat=posCnt)
            for var in product(self.bases, repeat=posCnt):
                seqlist = list(seq)
                for t in zip(posArr, var):
                    seqlist[t[0]] = t[1]
                    newSeq = "".join(seqlist)
                    ## newSeq not in dict, or exists newSeq but with same ID
                    if newSeq not in self.misDict or self.misDict[newSeq] == self.misDict[seq]:
                        self.misDict[newSeq] = idx
                    else:
                        self.misDict[newSeq] = "ambiguous"

    def createMisDict(self, misArr):
        """
            Create misDict for all barcodes
        """
        misPosIdxArr = self._getMisPosArr(misArr)
        for b in self.barcode:
            self.createMis(b, self.barcode[b], misPosIdxArr)

class SplitBarcode(object):
    """ docstring of SplitBarcode """

    def __init__(self):
        # super(self.__class__, self).__init__()
        self.maxBuffer = 5 * 1e9

    def _mergeIdx(self, idxParam, readLen1=0, readLen2=0):
        mergedArr = []
        for e in idxParam:
            if e[0] > 0:
                realPos = e[0] - 1 - readLen1
            else:
                realPos = e[0] + readLen2
            if not mergedArr or mergedArr[-1][0] + mergedArr[-1][1] != realPos:
                mergedArr.append([realPos, e[1]])
            else:
                mergedArr[-1][1] += e[1]
        return mergedArr

    def _demulexMultiple(self, seq, indexList):
        cand = ""
        for p in indexList:
            cand += seq[p[0]:p[0]+p[1]]
        return cand

    def _demulexSingle(self, seq, indexList):
        return seq[indexList[0][0]:indexList[0][0] + indexList[0][1]]

    def _remergeMultiple(self, seq, indexList):
        cand = ""
        st = 0
        for p in indexList:
            cand += seq[st:p[0]]
            st = p[0] + p[1]
        if len(seq) < st:
            cand += seq[st:]
        return cand

    def _remergeSingle(self, seq, indexList):
        return seq[:indexList[0][0]] + seq[indexList[0][0] + indexList[0][1]:]

    def createOutputFiles(self, prefix, barcode, outDir=None, isPE=False, compress=True):
        fhDict1 = {}
        fhDict2 = {}
        if not outDir:
            outDir = os.getcwd()

        if compress:
            openFunc = lambda x: gzip.open(x, "wt")
        else:
            bufferSize = self.maxBuffer // (len(barcode) + 2)
            if isPE:
                bufferSize = bufferSize // 2
            bufferSize = int(bufferSize)
            openFunc = lambda x: open(x, "w", bufferSize)

        buckets = list(barcode.keys()) + ["ambiguous", "undecoded"]

        for b in buckets:
            base = os.path.join(outDir, prefix + "_{:s}".format(b) + "{:s}.fq")
            if compress:
                base += ".gz"
            if isPE:
                fn1 = base.format("_1")
                fn2 = base.format("_2")
                fhDict1[b] = openFunc(fn1)
                fhDict2[b] = openFunc(fn2)
            else:
                fn1 = base.format("")
                fhDict1[b] = openFunc(fn1)

        return (fhDict1, fhDict2)

    def createStat(self, cntStat, barcode, misDict, outDir=None):
        if not outDir:
            outDir = os.getcwd()

        revDict = {barcode[x]: x for x in barcode}
        barcodeStat = {}

        totalCount = sum(cntStat.values())
        with open(os.path.join(outDir, "SequenceStat.txt"), 'w') as fh:
            fh.write("#Sequence\tBarcode\tCount\tPercentage\n")
            for s in sorted(cntStat.keys(), key=lambda x: cntStat[x], reverse=True):
                tag = misDict.get(s, "undecoded")
                fh.write("{:s}\t{:s}\t{:d}\t{:.6f}\n".format(s, tag, cntStat[s], 100 * cntStat[s] / totalCount))

                if tag not in barcodeStat:
                    barcodeStat[tag] = [0, 0]
                if s in revDict:
                    barcodeStat[tag][0] += cntStat[s]
                else:
                    barcodeStat[tag][1] += cntStat[s]

        with open(os.path.join(outDir, "BarcodeStat.txt"), 'w') as fh:
            fh.write("#Barcode\tCorrect\tCorrected\tTotal\tPercentage\n")
            for b in sorted(barcode.keys(), key=lambda x: (len(x), x)):
                tcnt = barcodeStat.get(b, [0, 0])
                total = tcnt[0] + tcnt[1]
                fh.write("{:s}\t{:d}\t{:d}\t{:d}\t{:.6f}\n".format(b, tcnt[0], tcnt[1], total, 100 * total / totalCount))
            correctSum = sum([e[0] for e in barcodeStat.values()]) - barcodeStat.get("undecoded", [0, 0])[0] - barcodeStat.get("ambiguous", [0, 0])[0]
            correctedSum = sum([e[1] for e in barcodeStat.values()]) - barcodeStat.get("undecoded", [0, 0])[1] - barcodeStat.get("ambiguous", [0, 0])[1]
            fh.write("Total\t{:d}\t{:d}\t{:d}\t{:.6f}\n".format(correctSum, correctedSum, correctSum + correctedSum, 100 * (correctSum + correctedSum) / totalCount))

    def decodePE(self, fq1, fq2, barcodeFile, indexParam, prefix=None, rc=False, outDir=None, compress=True):
        cntStat = {}
        bc = Barcode(barcodeFile)
        if rc:
            bc.reverseComplement()
        bc.createMisDict([(x[1], x[2]) for x in indexParam])

        decodeDict = bc.misDict

        if not prefix:
            idx = os.path.basename(fq1).find(".")
            if idx != -1:
                prefix = os.path.basename(fq1)[:idx]
            else:
                prefix = os.path.basename(fq1)
            if fq2:
                ## remove "_read_1"
                prefix = prefix[:-7]
            else:
                ## remove "_read"
                prefix = prefix[:-5]

        fhDict1, fhDict2 = self.createOutputFiles(prefix, bc.barcode, outDir=outDir, isPE=True, compress=compress)

        fh1 = gzip.open(fq1, "rt")
        head1 = fh1.readline()
        seq1 = fh1.readline()
        plus1 = fh1.readline()
        qual1 = fh1.readline()
        seqLen1 = len(seq1.strip())

        fh2 = gzip.open(fq2, "rt")
        head2 = fh2.readline()
        seq2 = fh2.readline()
        plus2 = fh2.readline()
        qual2 = fh2.readline()
        seqLen2 = len(seq2.strip())

        ## merge index if possible
        mergedIdx = self._mergeIdx(indexParam, seqLen1, readLen2=seqLen2)
        if len(mergedIdx) < 2:
            demulexFunc = self._demulexSingle
            remergeFunc = self._remergeSingle
        else:
            demulexFunc = self._demulexMultiple
            remergeFunc = self._remergeMultiple

        readCnt = 0
        while head2:
            ## assume that all index on read2
            barSeq = demulexFunc(seq2, mergedIdx)
            tag = decodeDict.get(barSeq, "undecoded")

            if tag == "ambiguous" or tag == "undecoded":
                newSeq2 = seq2
                newQual2 = qual2
            else:
                newSeq2 = remergeFunc(seq2, mergedIdx)
                newQual2 = remergeFunc(qual2, mergedIdx)

            ofh2 = fhDict2[tag]
            ofh2.write(head2)
            ofh2.write(newSeq2)
            ofh2.write(plus2)
            ofh2.write(newQual2)

            ## read next line
            head2 = fh2.readline()
            seq2 = fh2.readline()
            plus2 = fh2.readline()
            qual2 = fh2.readline()

            ## read1
            ofh1 = fhDict1[tag]
            ofh1.write(head1)
            ofh1.write(seq1)
            ofh1.write(plus1)
            ofh1.write(qual1)

            ## read next line
            head1 = fh1.readline()
            seq1 = fh1.readline()
            plus1 = fh1.readline()
            qual1 = fh1.readline()

            ## statistics
            if barSeq in cntStat:
                cntStat[barSeq] += 1
            else:
                cntStat[barSeq] = 1

            # readCnt += 1
            # if readCnt > 50000:
            #     break
        self.createStat(cntStat, bc.barcode, bc.misDict, outDir=outDir)

##########################################################################
############################  BEGIN Function  ############################
##########################################################################


######################################################################
############################  BEGIN Main  ############################
######################################################################
#################################
##
##   Main function of program.
##
#################################
def main():

    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage=USAGE)
    ArgParser.add_argument("--version", action="version", version=PROG_VERSION)
    ArgParser.add_argument("-2", action="store", dest="fq2", default=None, metavar="FILE", help="Fastq for PE mode. [%(default)s]")
    ArgParser.add_argument("-o", "--outDir", action="store", dest="outDir", default=None, metavar="DIR", help="Output dir for decoded fastq. [%(default)s]")
    ArgParser.add_argument("-n", "--noCompress", action="store_true", dest="noCompress", default=False, help="Output fastq in plain text instead gz format. [%(default)s]")
    ArgParser.add_argument("-b", "--barcodeInfo", action="append", dest="barcodeInfo", type=int, default=None, metavar="INT", nargs=3, help="Barcode information: startCycle, length, mismatchNum. [Last 10 cycle with 1 mismatch]")
    ArgParser.add_argument("-r", "--reverseComplement", action="store_true", dest="rc", default=False, help="Apply reverse complement of barcode sequence. [%(default)s]")

    (para, args) = ArgParser.parse_known_args()

    if len(args) != 2:
        ArgParser.print_help()
        print("\n[ERROR]: The parameters number is not correct!", file=sys.stderr)
        sys.exit(1)
    else:
        (barcodeFile, fastq1) = args

    ############################# Main Body #############################
    if not para.barcodeInfo:
        para.barcodeInfo = [[-10, 10, 1]]
    if para.outDir and not os.path.exists(para.outDir):
        os.makedirs(para.outDir)

    sb = SplitBarcode()
    if para.fq2:
        sb.decodePE(fastq1, para.fq2, barcodeFile, para.barcodeInfo, outDir=para.outDir, rc=para.rc, compress=not para.noCompress)
        # sb.decodePE(fastq1, para.fq2, barcodeFile, [(201, 10, 2)], outDir=para.outDir, rc=True, compress=not para.noCompress)
    # bar = Barcode(barcodeFile)
    # bar.createMisDict([(5, 2)])
    # print(bar.misDict)


    return 0

#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    return_code = main()
    sys.exit(return_code)

################## God's in his heaven, All's right with the world. ##################
