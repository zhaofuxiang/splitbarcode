//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include "SplitBarcode.h"
#include <iostream>
using namespace std;

#include "Utils.h"
#include "Timer.h"
#include <regex>
#include <boost/filesystem.hpp>
#include <algorithm>

SplitBarcode::SplitBarcode()
{
    
    m_end = false;

	m_isPE = false;
	m_read1_length = 0;
	m_read2_length = 0;
	m_prefix = "FS2000L1";
	slide = "FS2000";
	lane = "1";
	maxThreads = 0;
	m_finishFastq.resize(2, false);
	fov_buffer_size = 0;

	m_uThread = 0;
	m_uMemory = 0.0;
}

SplitBarcode::~SplitBarcode()
{
	if (maxThreads != 0)
	{
		for (int i = 0; i < maxThreads; ++i)
		{
			m_info[i].buff1.shrink_to_fit();
			m_info[i].buff2.shrink_to_fit();
		}
		delete[] m_info;
	}

	if (fov_buffer_size != 0)
	{
		for (int i = 0; i < 2; ++i)
		{
			for (int j = 0; j < fov_buffer_size; ++j)
				m_fovBuffer[i][j].buff.shrink_to_fit();
			delete[] m_fovBuffer[i];
		}
	}
}

bool SplitBarcode::transBarcodeInfo(string & barcodeInfo)
{
	m_barcode_info.clear();

	string tmpBarcodeInfo = barcodeInfo;
	if (tmpBarcodeInfo == "")
	{
		if (m_isPE)
			m_barcode_info.push_back(m_read2_length - 10);
		else
			m_barcode_info.push_back(m_read1_length - 10);
		m_barcode_info.push_back(10);
		m_barcode_info.push_back(1);
	}
	else
	{
		std::string sep = ",";
		size_t pos;
		while ((pos = tmpBarcodeInfo.find(sep)) != string::npos)
		{
			m_barcode_info.push_back(atoi(tmpBarcodeInfo.substr(0, pos).c_str()));
			tmpBarcodeInfo = tmpBarcodeInfo.substr(pos + 1, tmpBarcodeInfo.size());
		}
		m_barcode_info.push_back(atoi(tmpBarcodeInfo.c_str()));
	}

#ifdef DEBUG
	for (int i = 0; i < m_barcode_info.size(); ++i)
		cout << m_barcode_info[i] << " ";
	cout << endl;
#endif

	/*b1 = m_barcode_info[0] - 100, l1 = m_barcode_info[1], b2 = m_barcode_info[3] - 100, l2 = m_barcode_info[4];
	cout << "b1 " << b1 << " l1 " << l1 << " b2 " << b2 << " l2 " << l2 << endl;*/


	if (m_barcode_info.size() == 3)
	{
		if (m_barcode_info[0] < 0 || m_barcode_info[0] + m_barcode_info[1] > m_read1_length + m_read2_length)
			return false;
		else
		{
			if (m_isPE)
				b1 = m_barcode_info[0] - m_read1_length, l1 = m_barcode_info[1], b2 = 0, l2 = 0;
			else
				b1 = m_barcode_info[0], l1 = m_barcode_info[1], b2 = 0, l2 = 0;
			return true;
		}
	}
	else if (m_barcode_info.size() == 6)
	{
		if (m_barcode_info[0] < 0 || m_barcode_info[0] + m_barcode_info[1] > m_barcode_info[3] ||
			m_barcode_info[3] + m_barcode_info[4] > m_read1_length + m_read2_length)
			return false;
		else
		{
			if (m_isPE)
				b1 = m_barcode_info[0] - m_read1_length, l1 = m_barcode_info[1], b2 = m_barcode_info[3] - m_read1_length, l2 = m_barcode_info[4];
			else
				b1 = m_barcode_info[0], l1 = m_barcode_info[1], b2 = m_barcode_info[3], l2 = m_barcode_info[4];
			return true;
		}
	}
	else
		return false;
}

void SplitBarcode::getReadLength(string &fastqFile1, string &fastqFile2)
{
	std::ifstream file1(fastqFile1, std::ios_base::in | std::ios_base::binary);
	io::filtering_istream in;
	in.push(io::gzip_decompressor());
	in.push(file1);

	std::string l;
	if (std::getline(in, l, '\n'))
	{
		string flag = l;
		smatch m;
		//regex e("^@(.*)L(\\d{1})C(\\d{3})R(\\d{3}).*");
		regex e("^@(.*)L(\\d{1})C(\\d{3})R(\\d{3}).*");
		if (regex_search(flag, m, e))
		{
			slide = m.format("$1");
			lane = m.format("$2");
		}

		regex e2("^(.*)C(\\d{3})R(\\d{3}).*");
		if (regex_search(flag, m, e2))
		{
			m_prefix = m.format("$1");
		}
	}

	if (std::getline(in, l, '\n'))
	{
		m_read1_length = l.size();
	}

	if (m_isPE)
	{
		std::ifstream file2(fastqFile2, std::ios_base::in | std::ios_base::binary);
		io::filtering_istream in2;
		in2.push(io::gzip_decompressor());
		in2.push(file2);
		if (std::getline(in2, l, '\n'))
			;
		if (std::getline(in2, l, '\n'))
		{
			m_read2_length = l.size();
		}
	}

	return;
}

bool SplitBarcode::init(string &fastqFile1, string &fastqFile2, string & barcodeFile, string & barcodeInfo, string & outDir, string & reverse)
{

#ifdef DEBUG
	cout << "fastqFile1 " << fastqFile1 << " fastqFile2 " << fastqFile2 <<
		" barcodeFile " << barcodeFile << " barcodeInfo " << barcodeInfo <<
		" outDir " << outDir << " reverse " << reverse << endl;
#endif
	// check parameters
	if (!boost::filesystem::exists(fastqFile1) || !boost::filesystem::exists(barcodeFile))
	{
		cout << "There are not exists " << fastqFile1 << " or " << barcodeFile << endl;
		return false;
	}

	if (fastqFile2 == "" || !boost::filesystem::exists(fastqFile2))
	{
		cout << "Use SE mode" << endl;
		m_isPE = false;
	}
	else
	{
		cout << "Use PE mode" << endl;
		m_isPE = true;
	}
	m_fastqFile1 = fastqFile1;
	m_fastqFile2 = fastqFile2;

	m_outputDir = outDir;
	if (m_outputDir == "")
	{
		boost::filesystem::path p(fastqFile1);
		boost::filesystem::path dir = p.parent_path();
		m_outputDir = dir.string();
	}
	create_directory(m_outputDir);

	getReadLength(fastqFile1, fastqFile2);
	if (!transBarcodeInfo(barcodeInfo))
	{
		cout << "BarcodeInfo error: " << barcodeInfo << endl;
		return false;
	}

	// create hash table
	m_barcode = Barcode(barcodeFile);
	if (m_barcode.lens != (l1 + l2))
	{
		cout << "barcode length: " << m_barcode.lens << " not equal split length: " << l1 + l2 << endl;
		return false;
	}

	if (reverse == "true")
		m_barcode.reverseComplement(m_barcode_info);
	m_barcode.createMisDict(m_barcode_info);

	// prepare to process
	vector<string> barcodes = m_barcode.getBarcodes();
	m_barcode_num = barcodes.size();
	m_fileMutexs.resize(m_barcode_num + 2);
	m_publicBuffMutexs.resize(m_barcode_num + 2);
	m_fastqBuffs.resize(m_barcode_num + 2, "");
	if (m_isPE)
		m_fastqBuffs2.resize(m_barcode_num + 2, "");
	// calSize * totalcycle * 3 / 2

	int dnb_number = 1400000;
	m_maxFastqBuff = (unsigned long long)dnb_number * m_read1_length * 3 / 8;
	m_outFastqBuff = m_maxFastqBuff / 5;
	m_maxFastqBuff2 = m_outFastqBuff2 = 0;
	if (m_isPE)
	{
		m_maxFastqBuff2 = (unsigned long long)dnb_number * m_read2_length * 3 / 8;
		m_outFastqBuff2 = m_maxFastqBuff2 / 5;
	}
#ifdef DEBUG
	cout << "m_maxFastqBuff(M) " << (long long)m_maxFastqBuff / (1024 * 1024) << " m_outFastqBuff " << m_outFastqBuff / (1024 * 1024) << endl;
	cout << "m_maxFastqBuff2(M) " << m_maxFastqBuff2 / (1024 * 1024) << " m_outFastqBuff2 " << m_outFastqBuff2 / (1024 * 1024) << endl;
#endif

	for (int i = 0; i < m_barcode_num + 2; ++i)
	{
		try
		{
			m_fileMutexs[i] = new mutex;
			m_publicBuffMutexs[i] = new mutex;
			m_fastqBuffs[i].reserve(m_maxFastqBuff + 1024 * 1024);
			if (m_isPE)
				m_fastqBuffs2[i].reserve(m_maxFastqBuff2 + 1024 * 1024);
		}
		catch (std::bad_alloc)
		{
			cout << "lack of memory, failed init buffer in filenum: " << i << endl;
			return false;
		}
	}

	char prefix[100];
	sprintf(prefix, "%s_L0%s", slide.c_str(), lane.c_str());
	if (!createOutputFiles(prefix, barcodes, m_outputDir, m_isPE))
		return false;


	// get available memory
	long long totalPhysMem, validPhysMem;
	getMemoryInfo(totalPhysMem, validPhysMem); // getting memery information
	cout << "system totalPhysMem: " << totalPhysMem / (1024 * 1024 * 1024) << "(Gb) validPhysMem " << validPhysMem / (1024 * 1024 * 1024) << "(Gb)" << endl;

	long long availableMemory = validPhysMem;
	if (m_uMemory != 0.0)
		availableMemory = (long long)(m_uMemory * 1024 * 1024 * 1024);

	cout << "available memory: " << availableMemory/(1024 * 1024 * 1024)<< "(GB)" << endl;
	int pe = m_isPE ? 2 : 1;
	int maxThreadsber = ((long long)availableMemory / (1024 * 1024 * 1024) - (2 + m_barcode_num * pe * 0.045)) / ((1.0*dnb_number*pe * 3 / (1024 * 1024 * 1024))*(m_read1_length + m_read2_length));
	if (maxThreadsber <= 0)
	{
		cout << "can not split barcode because of lacking memory" << endl;
		return false;
	}

	maxThreads = get_CPU_core_num();
	if (maxThreadsber < maxThreads)
		maxThreads = maxThreadsber;

	if (m_uThread != 0)
		maxThreads = min(maxThreads, m_uThread);

	cout << "using thread number: " << maxThreads << endl;

	m_info = new DataInfo[maxThreads];
	for (int i = 0; i < maxThreads; ++i)
	{
		m_info[i].cycle = 0;
		m_info[i].fov = "";
	}

	fov_buffer_size = 5;
	m_fovBuffer.resize(2);
	for (int i = 0; i < 2; ++i)
	{
		FovInfo * tmpFovInfo = new FovInfo[fov_buffer_size];
		for (int j = 0; j < fov_buffer_size; ++j)
		{
			tmpFovInfo[j].cycle = 0;
			tmpFovInfo[j].fov = "";
		}
		m_fovBuffer[i] = tmpFovInfo;
	}

	

	return true;
}

void SplitBarcode::decodePE()
{
	// process: read fastq file line-by-line and get whole fov data
	/*if (m_isPE)
		process(m_fastqFile1, m_fastqFile2);
	else
		process(m_fastqFile1);*/

	while (true)
	{
		if (m_finishFastq[0] && m_finishFastq[1])
		{
			bool is_finish = true;
			for (int i = 0; i < fov_buffer_size; ++i)
			{
				if (m_fovBuffer[0][i].cycle != 0 || m_fovBuffer[1][i].cycle != 0)
				{
					is_finish = false;
					break;
				}
			}
			if (is_finish)
				break;
		}

		// pe mode
		string fov = "";
		int i = 0;
		for (; i < fov_buffer_size; ++i)
		{
			if (m_fovBuffer[0][i].cycle != 0)
			{
				fov = m_fovBuffer[0][i].fov;
				break;
			}
		}
		if (i == fov_buffer_size)
		{
			this_thread::sleep_for(chrono::microseconds(10));
			continue;
		}

		int j = 0;
		if (m_isPE)
		{
			for (; j < fov_buffer_size; ++j)
			{
				if (m_fovBuffer[1][j].cycle != 0 && m_fovBuffer[1][j].fov == fov)
					break;
			}
			if (j == fov_buffer_size)
			{
				this_thread::sleep_for(chrono::microseconds(10));
				continue;
			}
		}

		// do the job
		while (true)
		{
			int k;
			for (k = 0; k < maxThreads; ++k)
			{
				if (m_info[k].cycle == 0)
				{
#ifdef DEBUG
					cout << "prepare fov " << fov << " 0:" << i << " 1:" << j << endl;
#endif
					if (m_isPE)
						m_info[k].buff2.assign(m_fovBuffer[1][j].buff.begin(), m_fovBuffer[1][j].buff.end());
					m_info[k].buff1.assign(m_fovBuffer[0][i].buff.begin(), m_fovBuffer[0][i].buff.end());
					m_info[k].fov = fov;
					m_info[k].cycle = m_fovBuffer[0][i].cycle + m_fovBuffer[1][j].cycle;

					if (m_isPE)
					{
						m_fovBuffer[1][j].buff.clear();
						m_fovBuffer[1][j].fov = "";
						m_fovBuffer[1][j].cycle = 0;
					}

					m_fovBuffer[0][i].fov = "";
					m_fovBuffer[0][i].buff.clear();
					m_fovBuffer[0][i].cycle = 0;
					break;
				}
			}
			if (k < maxThreads)
				break;
			else
			{
				this_thread::sleep_for(chrono::microseconds(10));
			}
		}
	}

	this_thread::sleep_for(chrono::seconds(1));
    m_end = true;

    return;
}

//std::vector< char > readline(gzFile f) {
//
//
//	std::vector< char > v(256);
//
//
//	unsigned pos = 0;
//
//
//	for (;; ) {
//
//
//		if (gzgets(f, &v[pos], v.size() - pos) == 0) {
//
//
//			// end-of-file or error
//
//
//			int err;
//
//
//			const char *msg = gzerror(f, &err);
//
//
//			if (err != Z_OK) {
//
//
//				// handle error
//
//
//			}
//
//
//			break;
//
//
//		}
//
//
//		unsigned read = strlen(&v[pos]);
//
//
//		if (v[pos + read - 1] == 'n') {
//
//
//			if (v[pos + read - 2] == 'r') {
//
//
//				pos = pos + read - 2;
//
//
//			}
//			else {
//
//
//				pos = pos + read - 1;
//
//
//			}
//
//
//			break;
//
//
//		}
//
//
//		if (read == 0 || pos + read < v.size() - 1) {
//
//
//			pos = read + pos;
//
//
//			break;
//
//
//		}
//
//
//		pos = v.size() - 1;
//
//
//		v.resize(v.size() * 2);
//
//
//	}
//
//
//	v.resize(pos);
//
//
//	return v;
//
//
//}

bool readline(gzFile f, string & l, int len) 
{
	//std::vector< char > v(256);
	string v(len, ' ');
	std::size_t pos = 0;
	
	if (gzgets(f, &v[pos], v.size()) == 0) 
	{
		// end-of-file or error
		int err;
		const char *msg = gzerror(f, &err);
		if (err != Z_OK) 
		{
			// handle error
			cout << "read gz file error, error_code: " << err << " error_msg: " << msg << endl;
			return false;
		}
		// cout << "err " << err << " msg "<<msg <<" data "<<v<< endl;
	}

	pos = v.find('\n');
	if (pos == string::npos)
	{
		//cout << "pos = 0" << endl;
		pos = 0;
	}

	v.resize(pos);
	l = v;

	v.clear();
	//return v;
	if (l.size() != 0)
		return true;
	//cout << "return false" << endl;
	return false;
}

void SplitBarcode::readFastq(string &fastqFile, int flag)
{
	// test my_read_line
	/*gzFile gzfp = gzopen(fastqFile.c_str(), "rb");

	Timer tt;
	std::vector< char > vec;
	vector<string> vec2;
	string s;
	while (true)
	{
		s = readline(gzfp);
		if (s.size() == 0)
			break;
		vec2.push_back(s);

	}
	cout << tt.toc() << endl;*/
	// for se mode, just ignore the second fastq file
	if (flag == 1 && !m_isPE)
	{
		m_finishFastq[flag] = true;
		return;
	}

	Timer timer;
	Timer t;

	/*std::ifstream file1(fastqFile, std::ios_base::in | std::ios_base::binary);
	io::filtering_istream in;
	in.push(io::gzip_decompressor());
	in.push(file1);*/

	gzFile gzfp = gzopen(fastqFile.c_str(), "rb");

	std::string l;
	int pos = 0;

	string curr_fov = "";
	long long line_num = 0;
	string tmp_fov;
	int len = max(m_read1_length, m_read2_length);
	len = max(len, int(m_prefix.size() + 8 + 7 + 2));
	len += 2;

	while (readline(gzfp, l, len))
	{
		// get prefix's size and first fov name
		if (line_num == 0)
			curr_fov = l.substr(m_prefix.size(), 8);

		// check current fov is same fov or a new fov?
		if (line_num % 4 == 0)
		{
			tmp_fov = l.substr(m_prefix.size(), 8);
			if (tmp_fov != curr_fov)
			{
				// new fov
				// process the current fov
	#ifdef DEBUG
				cout << "new fov" << flag << " " << curr_fov << " time " << t.toc()<<" s "<< m_fovBuffer[flag][pos].buff.size() << " cycle " << m_fovBuffer[flag][pos].buff[1].size() << " pos " << pos << endl;
	#endif
				m_fovBuffer[flag][pos].fov = curr_fov;
				m_fovBuffer[flag][pos].cycle = m_fovBuffer[flag][pos].buff[1].size();

				curr_fov = tmp_fov;
				
				while (true)
				{
					int i;
					for (i = 0; i < fov_buffer_size; ++i)
					{
						if (i == pos)
							continue;
						if (m_fovBuffer[flag][i].cycle == 0)
						{	
							// test: just read
							/*m_fovBuffer[flag][pos].cycle = 0;
							m_fovBuffer[flag][pos].fov = "";
							m_fovBuffer[flag][pos].buff.clear();*/
							pos = i;
							break;
						}
					}
					if (i < fov_buffer_size)
						break;
					else
						this_thread::sleep_for(chrono::microseconds(10));
				}			
			}
		}

		m_fovBuffer[flag][pos].buff.push_back(l);
		line_num++;
	}


	m_fovBuffer[flag][pos].fov = curr_fov;
	m_fovBuffer[flag][pos].cycle = m_fovBuffer[flag][pos].buff[1].size();
#ifdef DEBUG
	cout << "line_num " << line_num << endl;
	cout << "new last fov" << flag << " " << curr_fov << " time " << t.toc() << " s " << m_fovBuffer[flag][pos].buff.size() << " cycle " << m_fovBuffer[flag][pos].buff[1].size() << " pos " << pos << endl;
	/*for (int i = 0; i < 16; ++i)
	{
		cout << m_fovBuffer[flag][pos].buff[i] << endl;
	}
	for (int i = m_fovBuffer[flag][pos].buff.size() - 16; i < m_fovBuffer[flag][pos].buff.size(); ++i)
	{
		cout << m_fovBuffer[flag][pos].buff[i] << endl;
	}*/
#endif

	cout << "read fastq file"<< flag+1 << " time(s): " << timer.toc() << endl;
	m_finishFastq[flag] = true;

	return;
}

void SplitBarcode::run(int thread_id)
{
    //cout<<"in run thread_id "<<thread_id<<endl;
    while (m_info[thread_id].cycle == 0)
    {
        this_thread::sleep_for(chrono::microseconds(10));
    }

    // process one fov
#ifdef DEBUG
    //cout<<"start run thread_id "<<thread_id<<" fov "<<m_info[thread_id].fov<<endl;
#endif
    splitSingleFov(thread_id);

    m_info[thread_id].cycle = 0;
#ifdef DEBUG
    //cout<<"end run thread_id "<<thread_id<<" fov "<<m_info[thread_id].fov<<endl;
#endif
    return;
}

void SplitBarcode::splitSingleFov(int thread_id)
{
	Timer timer;

	string *raw_sequence1 = m_info[thread_id].buff1.data();
	string *raw_sequence2 = m_info[thread_id].buff2.data();
	int buff1_size = m_info[thread_id].buff1.size();
	int buff2_size = m_info[thread_id].buff2.size();
	/*vector<string> raw_sequence1 = m_info[thread_id].buff1;
	vector<string> raw_sequence2 = m_info[thread_id].buff2;*/
#ifdef DEBUG
    cout<<"split fov "<< m_info[thread_id].fov<<" s1 "<< buff1_size << " s2 " << buff2_size <<endl;
#endif

    // traverse vector, split barcode
    //for (int i = 0; i+3 < raw_sequence.size(); i+=4)
    //{
    //    out_string += raw_sequence[i]+"\n";
    //    out_string += raw_sequence[i+1]+"\n";
    //    out_string += raw_sequence[i+2]+"\n";
    //    out_string += raw_sequence[i+3]+"\n";
    //}
    //cout<<out_string;
    if (m_isPE && (buff1_size != buff2_size))
    {
        cout<<"splitSingleFov size error: "<< buff1_size <<" "<< buff2_size <<endl;
        return;
    }

	if (buff1_size == 0 || (m_isPE && buff2_size == 0))
	{
		cout << "splitSingleFov size error: " << buff1_size << " " << buff2_size << endl;
		return;
	}


    int total = 0, splited = 0;
    // traverse vector, split barcode
    string seq;
    string score;
    string out_string, out_string2;
    string barcode_sequence;
    int barcode_number;

    vector<string> fastq_buffs, fastq_buffs2;
    fastq_buffs.resize(m_barcode_num+2);
	if (m_isPE)
		fastq_buffs2.resize(m_barcode_num + 2);
	for (int i = 0; i < m_barcode_num + 2; ++i)
	{
		fastq_buffs[i].reserve((unsigned long long)buff1_size * 3 * m_read1_length / (m_barcode_num * 4));
		if (m_isPE)
			fastq_buffs2[i].reserve((unsigned long long)buff2_size * 3 * m_read2_length/(m_barcode_num*4));
	}

	if (!m_isPE)
	{
		for (int i = 0; i + 3 < buff1_size; i += 4)
		{
			out_string = "";
			
			seq = raw_sequence1[i + 1];
			if (b2 != 0)
				barcode_sequence = seq.substr(b1, l1) + seq.substr(b2, l2);
			else
				barcode_sequence = seq.substr(b1, l1);
			barcode_number = m_barcode.searchBarcdeNumber(barcode_sequence);
			if (barcode_number == 0 || barcode_number == m_barcode_num + 1)
			{
				//cout<<"barcode_number "<<barcode_number<<endl;
				out_string += raw_sequence1[i] + "\n";
				out_string += raw_sequence1[i + 1] + "\n";
				out_string += raw_sequence1[i + 2] + "\n";
				out_string += raw_sequence1[i + 3] + "\n";
			}
			else
			{
				splited++;
				//cout<<"barcode_number "<<barcode_number<<endl;
				out_string += raw_sequence1[i] + "\n";
				if (b2 != 0)
					out_string += seq.substr(0, b1) + seq.substr(b1 + l1, b2 - b1 - l1) + seq.substr(b2 + l2, seq.size() - b2 - l2) + "\n";
				else
					out_string += seq.substr(0, b1) + seq.substr(b1 + l1, seq.size() - b1 - l1) + "\n";
				out_string += raw_sequence1[i + 2] + "\n";
				score = raw_sequence1[i + 3];
				if (b2 != 0)
					out_string += score.substr(0, b1) + score.substr(b1 + l1, b2 - b1 - l1) + score.substr(b2 + l2, score.size() - b2 - l2) + "\n";
				else
					out_string += score.substr(0, b1) + score.substr(b1 + l1, seq.size() - b1 - l1) + "\n";
			}

			fastq_buffs[barcode_number].append(out_string);
			total++;
		}
#ifdef DEBUG
		cout << m_info[thread_id].fov << " splite rate " << float(splited) / total << " time(s) " << timer.toc() << endl;
#endif

		for (int i = 0; i < m_barcode_num + 2; ++i)
		{
			if (!fastq_buffs[i].empty())
			{
				try
				{
					m_fqStat.StatInfo(fastq_buffs[i].c_str(), i, 1);
					pushReadToshareBuff(fastq_buffs[i], i);
					fastq_buffs[i].shrink_to_fit();
				}
				catch (...)
				{
					cout << "SE exception" << endl;
					return;
				}
			}
		}
	}
	else
	{
		for (int i = 0; i + 3 < buff1_size; i += 4)
		{
			out_string = "";
			out_string2 = "";

			out_string += raw_sequence1[i] + "\n";
			out_string += raw_sequence1[i + 1] + "\n";
			out_string += raw_sequence1[i + 2] + "\n";
			out_string += raw_sequence1[i + 3] + "\n";

			seq = raw_sequence2[i + 1];
			if (b2 != 0)
				barcode_sequence = seq.substr(b1, l1) + seq.substr(b2, l2);
			else
				barcode_sequence = seq.substr(b1, l1);
			barcode_number = m_barcode.searchBarcdeNumber(barcode_sequence);
			if (barcode_number == 0 || barcode_number == m_barcode_num + 1)
			{
				//cout<<"barcode_number "<<barcode_number<<endl;
				out_string2 += raw_sequence2[i] + "\n";
				out_string2 += raw_sequence2[i + 1] + "\n";
				out_string2 += raw_sequence2[i + 2] + "\n";
				out_string2 += raw_sequence2[i + 3] + "\n";
			}
			else
			{
				splited++;
				//cout<<"barcode_number "<<barcode_number<<endl;
				out_string2 += raw_sequence2[i] + "\n";
				if (b2 != 0)
					out_string2 += seq.substr(0, b1) + seq.substr(b1 + l1, b2 - b1 - l1) + seq.substr(b2 + l2, seq.size() - b2 - l2) + "\n";
				else
					out_string2 += seq.substr(0, b1) + seq.substr(b1 + l1, seq.size() - b1 - l1) + "\n";
				out_string2 += raw_sequence2[i + 2] + "\n";
				score = raw_sequence2[i + 3];
				if (b2 != 0)
					out_string2 += score.substr(0, b1) + score.substr(b1 + l1, b2 - b1 - l1) + score.substr(b2 + l2, score.size() - b2 - l2) + "\n";
				else
					out_string2 += score.substr(0, b1) + score.substr(b1 + l1, score.size() - b1 - l1) + "\n";
			}

			fastq_buffs[barcode_number].append(out_string);
			fastq_buffs2[barcode_number].append(out_string2);

			total++;
		}
#ifdef DEBUG
		cout << m_info[thread_id].fov  << " splite rate " << float(splited) / total << " time(s) " << timer.toc() << endl;
#endif

		for (int i = 0; i < m_barcode_num + 2; ++i)
		{
			if (!fastq_buffs[i].empty())
			{
				try
				{
					m_fqStat.StatInfo(fastq_buffs[i].c_str(), i, 1);
					m_fqStat.StatInfo(fastq_buffs2[i].c_str(), i, 2);
					pushPEReadToshareBuff(fastq_buffs[i], fastq_buffs2[i], i);
					fastq_buffs[i].shrink_to_fit();
					fastq_buffs2[i].shrink_to_fit();
				}
				catch (...)
				{
					cout << "PE exception" << endl;
					return;
				}
			}
		}
#ifdef DEBUG
		cout << m_info[thread_id].fov << " compress and write disk time(s) " << timer.toc() << endl;
#endif
	}
    //cout<<out_string2;
}


void SplitBarcode::process(string &fastqFile1, string &fastqFile2)
{
	/*Timer ttt;
	gzFile gzfp = gzopen(fastqFile1.c_str(), "rb");
	if (!gzfp)
	return;
	gzFile gzfp2 = gzopen(fastqFile2.c_str(), "rb");
	if (!gzfp2)
	return;

	string raw_string, raw_string2;
	string line;
	vector<string> raw_sequence, raw_sequence_last2, raw_sequence_curr2;
	int buffer_size = 100 * 1024 * 1024;

	unsigned char* buf = (unsigned char*)malloc(sizeof(unsigned char)*buffer_size);
	unsigned char* buf2 = (unsigned char*)malloc(sizeof(unsigned char)*buffer_size);
	int read_length, read_length2;
	vector<string> fov_sequence1, fov_sequence2;
	while ((read_length = gzread(gzfp, buf, buffer_size)) > 0 &&
	(read_length2 = gzread(gzfp2, buf2, buffer_size)) > 0)
	{
	raw_string.append((const char*)buf, read_length);
	raw_string2.append((const char*)buf2, read_length2);
	}
	cout << ttt.toc() << endl;
	split(raw_string, '\n', raw_sequence);
	split(raw_string2, '\n', raw_sequence_curr2);
	cout << "size " << raw_sequence.size() << " " << raw_sequence_curr2.size() << endl;
	cout << ttt.toc() << endl;*/

	Timer timer;
	Timer t;
	Timer tt;

	std::ifstream file1(fastqFile1, std::ios_base::in | std::ios_base::binary);
	io::filtering_istream in;
	in.push(io::gzip_decompressor());
	in.push(file1);

	std::ifstream file2(fastqFile2, std::ios_base::in | std::ios_base::binary);
	io::filtering_istream in2;
	in2.push(io::gzip_decompressor());
	in2.push(file2);

	std::string l, ll;
	vector<string> tmp, tmp2;

	string curr_fov = "";
	long long line_num = 0;
	string tmp_fov;

	tt.tic();
	vector<string> vec;
	while (std::getline(in, l, '\n'))// && std::getline(in2, ll, '\n'))
	{
		vec.push_back(l);
	}
	cout << "just read gzip fq time " << tt.toc() << endl;
	while (std::getline(in2, ll, '\n'))// && std::getline(in2, ll, '\n'))
	{
	}
	cout << "just read gzip fq time " << tt.toc() << endl;

	while (std::getline(in, l, '\n') && std::getline(in2, ll, '\n'))
	{
		// get prefix's size and first fov name
		if (line_num == 0)
		{
			string flag = l;
			smatch m;
			//regex e("^@(.*)L(\\d{1})C(\\d{3})R(\\d{3}).*");
			regex e("^(.*)C(\\d{3})R(\\d{3}).*");
			if (regex_search(flag, m, e))
			{
				m_prefix = m.format("$1");
				curr_fov = flag.substr(m_prefix.size(), 8);
			}
			else
			{
				cout << "unknown fastq format!" << endl;
				return;
			}
		}

		// check current fov is same fov or a new fov?
		if (line_num % 4 == 0)
		{
			tmp_fov = l.substr(m_prefix.size(), 8);
			if (tmp_fov != curr_fov)
			{
				// new fov
				// process the current fov
				while (true)
				{
					int i;
					for (i = 0; i < maxThreads; ++i)
					{
						if (m_info[i].cycle == 0)
						{
							m_info[i].fov = curr_fov;
							tt.tic();
							m_info[i].buff1.assign(tmp.begin(), tmp.end());
							m_info[i].buff2.assign(tmp2.begin(), tmp2.end());
#ifdef DEBUG
							cout << "assign time " << tt.toc() << endl;
#endif
#ifdef DEBUG
							cout << "get new fov " << curr_fov << " times(s) " << t.toc() << endl;
#endif
							m_info[i].cycle = tmp[1].size() + tmp2[1].size();

							break;
						}
					}
					if (i < maxThreads)
						break;
					else
					{
#ifdef DEBUG
						cout << "sleep for waiting processing m_info" << endl;
#endif
						this_thread::sleep_for(chrono::microseconds(10));
					}
				}

				tmp.clear();
				tmp2.clear();
				curr_fov = tmp_fov;
			}
		}

		tmp.push_back(l);
		tmp2.push_back(ll);
		line_num++;
	}

	while (true)
	{
		int i;
		for (i = 0; i < maxThreads; ++i)
		{
			if (m_info[i].cycle == 0)
			{
				m_info[i].fov = curr_fov;
				tt.tic();
				m_info[i].buff1.assign(tmp.begin(), tmp.end());
				m_info[i].buff2.assign(tmp2.begin(), tmp2.end());
#ifdef DEBUG
				cout << "assign time " << tt.toc() << endl;
#endif
#ifdef DEBUG
				cout << "get new fov " << curr_fov << " times(s) " << t.toc() << endl;
#endif
				m_info[i].cycle = tmp[1].size() + tmp2[1].size();
				break;
			}
		}
		if (i < maxThreads)
			break;
		else
		{
			//cout<<"sleep for waiting processing m_info"<<endl;
			this_thread::sleep_for(chrono::microseconds(10));
		}
	}

	tmp.shrink_to_fit();
	tmp2.shrink_to_fit();

	cout << "read fastq files time(s): " << timer.toc() << endl;

	return;
}

void SplitBarcode::process(string &fastqFile1)
{
	Timer t;

	std::ifstream file1(fastqFile1, std::ios_base::in | std::ios_base::binary);
	io::filtering_istream in;
	in.push(io::gzip_decompressor());
	in.push(file1);

	std::string l;
	vector<string> tmp;

	string curr_fov = "";
	long long line_num = 0;
	string tmp_fov;
	while (std::getline(in, l, '\n'))
	{
		// get prefix's size and first fov name
		if (line_num == 0)
		{
			string flag = l;
			smatch m;
			//regex e("^@(.*)L(\\d{1})C(\\d{3})R(\\d{3}).*");
			regex e("^(.*)C(\\d{3})R(\\d{3}).*");
			if (regex_search(flag, m, e))
			{
				m_prefix = m.format("$1");
				curr_fov = flag.substr(m_prefix.size(), 8);
			}
			else
			{
				cout << "unknown fastq format!" << endl;
				return;
			}
		}

		// check current fov is same fov or a new fov?
		if (line_num % 4 == 0)
		{
			tmp_fov = l.substr(m_prefix.size(), 8);
			if (tmp_fov != curr_fov)
			{
				// new fov
				// process the current fov
				while (true)
				{
					int i;
					for (i = 0; i < maxThreads; ++i)
					{
						if (m_info[i].cycle == 0)
						{
							m_info[i].fov = curr_fov;
							m_info[i].buff1.assign(tmp.begin(), tmp.end());
							m_info[i].buff2.clear();
							m_info[i].cycle = tmp[1].size();
#ifdef DEBUG
							cout << "new fov " << curr_fov << " cycle " << m_info[i].cycle << endl;
#endif
							break;
						}
					}
					if (i < maxThreads)
						break;
					else
					{
#ifdef DEBUG
						cout << "sleep for waiting processing m_info" << endl;
#endif
						this_thread::sleep_for(chrono::microseconds(10));
					}
				}

				tmp.clear();
				curr_fov = tmp_fov;
			}
		}

		tmp.push_back(l);
		line_num++;
	}

	while (true)
	{
		int i;
		for (i = 0; i < maxThreads; ++i)
		{
			if (m_info[i].cycle == 0)
			{
				m_info[i].fov = curr_fov;
				m_info[i].buff1.assign(tmp.begin(), tmp.end());
				m_info[i].buff2.clear();
				m_info[i].cycle = tmp[1].size();
#ifdef DEBUG
				cout << "new fov " << curr_fov << " cycle " << m_info[i].cycle << endl;
#endif
				break;
			}
		}
		if (i < maxThreads)
			break;
		else
		{
			//cout<<"sleep for waiting processing m_info"<<endl;
			this_thread::sleep_for(chrono::microseconds(10));
		}
	}

	tmp.shrink_to_fit();

	cout << "read fastq files time(s): " << t.toc() << endl;

	return;
}

/*
void SplitBarcode::process(string &fastqFile1, string &fastqFile2, vector<int> barcodeInfo)
{
    gzFile gzfp = gzopen(fastqFile1.c_str(), "rb");
    if (!gzfp)
        return;
    gzFile gzfp2 = gzopen(fastqFile2.c_str(), "rb");
    if (!gzfp2)
        return;

    string raw_string, raw_string2;
    string line;
    vector<string> raw_sequence, raw_sequence_last2, raw_sequence_curr2;
    int buffer_size = 300 * 1024 * 1024;
    //int buffer_size = 500;
    //unsigned char buf[buffer_size], buf2[buffer_size];
    unsigned char* buf = (unsigned char*)malloc(sizeof(unsigned char)*buffer_size);
    unsigned char* buf2 = (unsigned char*)malloc(sizeof(unsigned char)*buffer_size);
    int read_length, read_length2;
    b1 = barcodeInfo[0]-100, l1 = barcodeInfo[1], b2 = barcodeInfo[3]-100, l2 = barcodeInfo[4];
    if (b1 + l1 > 220 || b2 + l2 > 220)
    {
        gzclose(gzfp);
        gzclose(gzfp2);
        cout<<"barcode cycle parameter is lager than real cycle"<<endl;
        return;
    }
    Timer timer;
    vector<pair<int, int>> fov_pos;
    vector<string> fov_sequence1, fov_sequence2;
    while (//(read_length = gzread(gzfp, buf, buffer_size)) > 0 || 
            (read_length2 = gzread(gzfp2, buf2, buffer_size)) > 0)
    {
        cout<<"read buffer time(s): "<<timer.toc()<<endl;
        // split raw data line-by-line to vector
        //raw_string.append((const char*)buf, read_length);
        //split(raw_string, '\n', raw_sequence);

        //cout<<"split buffer1 line-by-line time(s): "<<timer.toc()<<endl;

        // split raw data line-by-line to vector
        raw_string2.append((const char*)buf2, read_length2);
        split(raw_string2, '\n', raw_sequence_curr2);
        cout<<"split buffer time(s): "<<timer.toc()<<endl;
        //cout<<"raw_string2"<<endl;
        cout<<"vec size "<<raw_sequence_curr2.size()<<endl;
        break;
        fov_pos = searchFovPosition(raw_sequence_last2, raw_sequence_curr2);
        //raw_sequence_curr2.clear();

        cout<<raw_string2<<endl;
        raw_string2.clear();

        cout<<"split buffer2 line-by-line time(s): "<<timer.toc()<<endl;

        for (int i = 0; i < fov_pos.size(); ++i)
        {
            int start = fov_pos[i].first;
            int end = fov_pos[i].second;
            fov_sequence1.assign(raw_sequence.begin()+start, raw_sequence.begin()+end);
            fov_sequence2.assign(raw_sequence_curr2.begin()+start, raw_sequence_curr2.begin()+end);
            splitSingleFov(fov_sequence1, fov_sequence2);
        }
    }
    // process last fov data
    // todo
    for (int i = 0; i < fov_pos.size(); ++i)
    {
        fov_sequence1.assign(raw_sequence.begin(), raw_sequence.begin());
        fov_sequence2.assign(raw_sequence_last2.begin(), raw_sequence_last2.begin());
        splitSingleFov(fov_sequence1, fov_sequence2);
    }
    
    free(buf);
    free(buf2);
    gzclose(gzfp);
    gzclose(gzfp2);
}
*/

bool SplitBarcode::createOutputFiles(string prefix, vector<string>& barcodes, string outDir, bool isPE)
{
#ifdef DEBUG
    cout<<"in createOutputFiles"<<endl;
#endif
    m_fastq_files.resize(barcodes.size()+2, NULL);
    if (isPE)
        m_fastq_files2.resize(barcodes.size()+2, NULL);

    FqstatInfo tempInfoBuff = { 0,0,0,0,0,0,0,0,0,0,"",{ 0 } };
    if (!isPE)
    {
        m_allFqStatInfo.resize(1, tempInfoBuff);
        m_allFqStatInfo[0].Name = outDir + "/" + prefix + std::string(".allfq");
    }
    else
    {
        m_allFqStatInfo.resize(2, tempInfoBuff);
        m_allFqStatInfo[0].Name = outDir + "/" + prefix + std::string("_1.allfq");
        m_allFqStatInfo[1].Name = outDir + "/" + prefix + std::string("_2.allfq");
    }

    // to do: add se version
    m_fqStat.InitstatQualInFq(static_cast<int>(barcodes.size()+2), 2);
    string label;
    string fq_name;
    for (int i = 0; i <= barcodes.size()+1; ++i)
    {
        if (i == 0)
            label = "undecoded";
        else if (i == barcodes.size()+1)
            label = "ambiguous";
        else
            label = barcodes[i-1];

        if (!isPE)
        {
            fq_name = outDir + "/" + prefix + "_" + label + ".fq.gz";
            m_fastq_files[i] = cmpOpen(fq_name.c_str());
            if (m_fastq_files[i] == NULL)
            {
                cout<<"can not open fastq file to write: "<<fq_name<<endl;
                return false;
            }
			m_fqStat.InitFqstatInfo(fq_name, i, 1);
        }
        else
        {
            fq_name = outDir + "/" + prefix + "_" + label + "_1.fq.gz";
            m_fastq_files[i] = cmpOpen(fq_name.c_str());
            if (m_fastq_files[i] == NULL)
            {
                cout<<"can not open fastq file to write: "<<fq_name<<endl;
                return false;
            }
            m_fqStat.InitFqstatInfo(fq_name, i, 1);

            fq_name = outDir + "/" + prefix + "_" + label + "_2.fq.gz";
            m_fastq_files2[i] = cmpOpen(fq_name.c_str());
            if (m_fastq_files2[i] == NULL)
            {
                cout<<"can not open fastq file to write: "<<fq_name<<endl;
                return false;
            }
            m_fqStat.InitFqstatInfo(fq_name, i, 2);
        }
        
                
    }
    return true;
}
vector<pair<int,int>> SplitBarcode::searchFovPosition(vector<string>& last, vector<string>& curr)
{
#ifdef DEBUG
    cout<<"in searchFovPosition"<<endl;
    cout<<"last size "<<last.size()<<endl;
    cout<<"curr size "<<curr.size()<<endl;
#endif

    vector<pair<int,int>> position;
    if (curr.size() <= 4 && last.size() == 0)
        return position;

    if (last.size() == 0)
    {
        if (m_fastq_size.size() == 0)
            for (int i = 0; i < 4; ++i)
                m_fastq_size.push_back(curr[i].size());

        // just ignore last data, it is simple
        string flag = curr[0];
        smatch m;
        //regex e("^@(.*)L(\\d{1})C(\\d{3})R(\\d{3}).*");
        regex e("^(.*)C(\\d{3})R(\\d{3}).*");
        if (regex_search(flag, m, e))
        {
			m_prefix = m.format("$1");
            //curr_fov = "C"+m.format("$2")+"R"+m.format("$3");
            //fov_list.push_back(curr_fov);
        }
        else
        {
            cout<<"unknown fastq format!"<<endl;
            return position;
        }
        position = doSearchFovPosition(last, curr);

    }
    else
    {
        
        // need process last data
        int boundary = last.size() % 4;
        bool merge = true;
        if (m_fastq_size[boundary] == last[last.size()-1].size())
            merge = false;

        vector<string>::iterator start_ite = curr.begin();
        if (merge)
        {
            last[last.size()-1] += curr[0];
            start_ite ++;
        }
        last.insert(last.end(), start_ite, curr.end());
        curr.clear();

        position = doSearchFovPosition(curr, last);
        last.swap(curr);
    }

    return position;
}

vector<pair<int,int>> SplitBarcode::doSearchFovPosition(vector<string>& last, vector<string>& curr)
{
    vector<pair<int,int>> position;
    //vector<string> fov_list;
    string curr_fov = curr[0].substr(m_prefix.size(), 8);
    int start=0, end=0;

    // search next fov
    string fov;
    for (int i = 0; i+3 < curr.size(); i+=4)
    {
        fov = curr[i].substr(m_prefix.size(),8);
        if (fov != curr_fov)
        {
            curr_fov = fov;
            //fov_list.push_back(fov);
            end = i;
            position.push_back(make_pair(start, end)); 
            start = i+1;
        }
    }
    cout<<"position"<<endl;
    for (int i = 0; i < position.size(); ++i)
        cout<<position[i].first<<" "<<position[i].second<<endl;

    cout<<"last"<<endl;
    for (int i = 0; i < last.size(); ++i)
        cout<<last[i]<<endl;
    cout<<"curr"<<endl;
    for (int i = 0; i < curr.size(); ++i)
        cout<<curr[i]<<endl;

    //cout<<"fov_list"<<endl;
    //for (int i = 0; i < fov_list.size(); ++i)
    //    cout<<fov_list[i]<<endl;

    cout<<"last size "<<last.size()<<endl;
    cout<<"curr size "<<curr.size()<<endl;
    if (end != 0) 
    {
        last.assign(curr.begin()+end, curr.end());
        curr.erase(curr.begin()+end, curr.end());
    }
    else
    {
        last.swap(curr);
    }
    cout<<"after process"<<endl;
    cout<<"last size "<<last.size()<<endl;
    cout<<"curr size "<<curr.size()<<endl;

    return position;
}

vector<bool> SplitBarcode::getReadyDataIndex()
{
    vector<bool> vec;
    for (int i = 0; i < maxThreads; ++i)
        if (m_info[i].cycle != 0)
            vec.push_back(true);
        else
            vec.push_back(false);


    return vec;
}

bool SplitBarcode::is_end()
{
    return m_end;
}

void SplitBarcode::pushReadToshareBuff(std::string &read1Buffer, const int fileNO)
{   
    try{
        size_t bufSize = read1Buffer.size();
        if (bufSize > m_outFastqBuff) //big enough to compress
            compData(read1Buffer.data(), bufSize, fileNO);
        else //add to fqbuff
        {           
            string compTemp;
            m_publicBuffMutexs[fileNO]->lock();
            m_fastqBuffs[fileNO] += read1Buffer;
            if (m_fastqBuffs[fileNO].size() > m_outFastqBuff)
            {
                compTemp.assign(m_fastqBuffs[fileNO]);
                m_fastqBuffs[fileNO].clear();
            }
            m_publicBuffMutexs[fileNO]->unlock();

            if (!compTemp.empty())
                compData(compTemp.data(), compTemp.size(), fileNO);
        }
        
    }
    catch (std::logic_error&) {
        std::cerr << __LINE__ << " [exception caught] pushReadToshareBuff" << std::endl;
    }
}

void SplitBarcode::pushPEReadToshareBuff(std::string &read1Buffer, std::string &read2Buffer, const int fileNO)
{
    try{
        if (read1Buffer.size() > m_outFastqBuff || read2Buffer.size() > m_outFastqBuff2)
        {
            compPEData(read1Buffer, read2Buffer, fileNO);
        }
        else
        {
            string compTemp, comp2Temp;
            m_publicBuffMutexs[fileNO]->lock();
            m_fastqBuffs[fileNO] += read1Buffer;
            m_fastqBuffs2[fileNO] += read2Buffer;
            if (m_fastqBuffs[fileNO].size() > m_outFastqBuff || m_fastqBuffs2[fileNO].size() > m_outFastqBuff2)
            {
                compTemp.assign(m_fastqBuffs[fileNO]);
                comp2Temp.assign(m_fastqBuffs2[fileNO]);
                m_fastqBuffs[fileNO].clear();
                m_fastqBuffs2[fileNO].clear();
            }
            m_publicBuffMutexs[fileNO]->unlock();

            
            if (!compTemp.empty())
                compPEData(compTemp, comp2Temp, fileNO);
        }       
    }
    catch (std::logic_error&) {
        std::cout << __LINE__ << " [exception caught] pushPEReadToshareBuff" << std::endl;
    }
}


void SplitBarcode::compPEData(const string&r1, const string&r2, const int fileNO)
{
    uLong buff1 = static_cast<uLong>(r1.size());
    uLong buff2 = static_cast<uLong>(r2.size());
    char *gzfq1 = (char*)malloc(buff1);
    char *gzfq2 = (char*)malloc(buff2);
    uLong compressedSize1 = buff1, compressedSize2 = buff2;
    if (gzcompress((Bytef*)r1.data(), buff1, (Bytef*)gzfq1, &compressedSize1) < 0|| gzcompress((Bytef*)r2.data(), buff2, (Bytef*)gzfq2, &compressedSize2) < 0)
    {
        cout<< __LINE__ << " failed to compress PE-readBuff, fileNO: " << fileNO <<endl;
        return;
    }

    m_fileMutexs[fileNO]->lock();
    gz_statep state = (gz_statep)m_fastq_files[fileNO];
    _write(state->fd, gzfq1, compressedSize1);
    state = (gz_statep)m_fastq_files2[fileNO];
    _write(state->fd, gzfq2, compressedSize2);
    m_fileMutexs[fileNO]->unlock();
    free(gzfq1); free(gzfq2);
}

int SplitBarcode::gzcompress(Bytef *data, uLong ndata, Bytef *zdata, uLong *nzdata)
{
    z_stream c_stream;
    int err = 0;

    if (data && ndata > 0) {
        c_stream.zalloc = NULL;
        c_stream.zfree = NULL;
        c_stream.opaque = NULL;     
        if (deflateInit2(&c_stream, Z_DEFAULT_COMPRESSION, Z_DEFLATED,
            MAX_WBITS + 16, 8, Z_DEFAULT_STRATEGY) != Z_OK) return -1; //deflateInit2_
        c_stream.next_in = data;
        c_stream.avail_in = ndata;
        c_stream.next_out = zdata;
        c_stream.avail_out = *nzdata;
        while (c_stream.avail_in != 0 && c_stream.total_out < *nzdata) {
            if (deflate(&c_stream, Z_NO_FLUSH) != Z_OK) return -1;
        }
        if (c_stream.avail_in != 0) return c_stream.avail_in;
        for (;;) {
            if ((err = deflate(&c_stream, Z_FINISH)) == Z_STREAM_END) break;
            if (err != Z_OK) return -1;
        }
        if (deflateEnd(&c_stream) != Z_OK) return -1;
        *nzdata = c_stream.total_out;
        return 0;
    }
    return -1;
}

void SplitBarcode::compData(const char *buff, const size_t bufSize, const int fileNO)//compress data and append to fqgzBuff
{
    char *gzfq = (char*)malloc(bufSize);
    uLong compressedSize = static_cast<uLong>(bufSize);
    if (gzcompress( (Bytef*)buff, static_cast<uLong>(bufSize), (Bytef*)gzfq, &compressedSize) < 0)
    {
        cout<< __LINE__ << " failed to compress SE-readBuff, fileNO: " << fileNO <<endl;
        return;
    }
    
    m_fileMutexs[fileNO]->lock();
    gz_statep state = (gz_statep)m_fastq_files[fileNO];
    _write(state->fd, gzfq, compressedSize);
    m_fileMutexs[fileNO]->unlock();
    free(gzfq);
}

void SplitBarcode::endFastq() //finished all m_fastqBuffer
{
	Timer timer;

	Timer t;
    cout << "go to endFastq" << endl;
    size_t fileNumber = m_fastqBuffs.size();
    for (int i = 0; i < fileNumber; ++i)
    {
        if (!m_fastqBuffs[i].empty()) // if empty() should write??? 
        {
            CWoneFastq(i);  //no need lock hear
        }
    }
#ifdef DEBUG
	cout << "gzputs time(s) " << t.toc() << endl;
#endif
    //output fqStat.txt and close all fq.gz
    //if (m_hasBarcode>0)
    //{
        m_barcode.statInfo(m_outputDir);  //output BarcodeStat and SequnceStat
        
        //if (m_FileName)
        //{
        //    fprintf(m_FileName, "BarcodeStat,%s\n", (m_outputDir + "/BarcodeStat.txt").c_str());
        //    fprintf(m_FileName, "SequenceStat,%s\n", (m_outputDir + "/SequenceStat.txt").c_str());
        //}

    //}
#ifdef DEBUG
		cout << "stat barcode time(s) " << t.toc() << endl;
#endif

    int FileNum = static_cast<int> (m_fastq_files.size());
    //if (m_read2 == 0) //SE
    //{
    //    for (int i = 0; i<FileNum; i++)
    //    {
    //        //m_fqStat.writeStatFile(i, 1);
    //        cmpClose(m_fastq_files[i]);
    //        delete m_fileMutexs[i];
    //        m_fileMutexs[i] = NULL;
    //        delete m_publicBuffMutexs[i];
    //        m_publicBuffMutexs[i] = NULL;
    //    }
    //    //m_fqStat.synAllStatInfo(m_allFqStatInfo[0], m_hasBarcode);
    //    //m_fqStat.writeAllfqStatFile(m_allFqStatInfo[0]);
    //}
    //else
    //{
        for (int i = 0; i < FileNum; i++)
        {
            m_fqStat.writeStatFile(i, 1);  // sequence_mode, 0
			if (m_isPE)
				m_fqStat.writeStatFile(i, 2);  //read2
            cmpClose(m_fastq_files[i]);
			if (m_isPE)
				cmpClose(m_fastq_files2[i]);
            delete m_fileMutexs[i];
            m_fileMutexs[i] = NULL;
            delete m_publicBuffMutexs[i];
            m_publicBuffMutexs[i] = NULL;
        }
        int m_hasBarcode = l1 + l2;
        m_fqStat.synAllStatInfo(m_allFqStatInfo, m_hasBarcode);
        m_fqStat.writeAllfqStatFile(m_allFqStatInfo[0]);
		if (m_isPE)
			m_fqStat.writeAllfqStatFile(m_allFqStatInfo[1]);
    //}
#ifdef DEBUG
		cout << "write fq stat time(s) " << t.toc() << endl;
#endif

#ifdef DEBUG
		cout << "end fastq time(s) " << timer.toc() << endl;
#endif
}

inline void SplitBarcode::CWoneFastq(const int fileNO)
{
    m_publicBuffMutexs[fileNO]->lock();
    cmpFunc(m_fastq_files[fileNO], m_fastqBuffs[fileNO].c_str());
    m_fastqBuffs[fileNO].clear(); 
    if (m_isPE)
    {
        cmpFunc(m_fastq_files2[fileNO], m_fastqBuffs2[fileNO].c_str());
        m_fastqBuffs2[fileNO].clear(); 
    }       
    m_publicBuffMutexs[fileNO]->unlock();   
}

int SplitBarcode::getThreadNumber()
{
	return maxThreads;
}

void SplitBarcode::setParameters(int thread, float memory)
{
	m_uThread = thread;
	m_uMemory = memory;
}
