//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#ifndef SPLIT_BARCOED_H
#define SPLIT_BARCOED_H

#include <string>
#include <vector>
using namespace std;

#include "Barcode.h"

#include "statQualInFq.hpp"

#include <mutex>
//#include <boost/thread/mutex.hpp>
//using boost::mutex;


// for read gzip file
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
namespace io = boost::iostreams;

#if defined(linux)
#include <unistd.h>
#define _write write
#endif

#include <zlib.h>
#include <gzguts.h>

#define cmpFile gzFile
#define cmpOpen(x) gzopen(x, "wb")
#define cmpClose(x) gzclose(x)
#define cmpFunc(x,y) gzputs(x,y)

typedef struct DataInfo
{
    int cycle;
    //int numBases;
    string fov;
    vector<string> buff1;
    vector<string> buff2;
}DInfo;

typedef struct FovInfo
{
	int cycle;
	//int numBases;
	string fov;
	vector<string> buff;
}FInfo;

class SplitBarcode
{
public:
    SplitBarcode();
    ~SplitBarcode();

    void decodePE();
	bool init(string &fastqFile1, string &fastqFile2, string & barcodeFile, string & barcodeInfo, string & outDir, string & reverse);
    void run(int thread_id);
    vector<bool> getReadyDataIndex();
    bool is_end();
    void endFastq();
	int getThreadNumber();
	void readFastq(string &fastqFile, int flag);
	void setParameters(int thread, float memory);

private:
    bool createOutputFiles(string prefix, vector<string>& barcodes, string outDir, bool isPE);
    void process(string &fastqFile1, string &fastqFile2);
	void process(string &fastqFile1);
    vector<pair<int,int>> searchFovPosition(vector<string>& last, vector<string>& curr);
    void splitSingleFov(int thread_id);
    vector<pair<int,int>> doSearchFovPosition(vector<string>& last, vector<string>& curr);
    void pushReadToshareBuff(std::string &read1Buffer, const int fileNO);
    void pushPEReadToshareBuff(std::string &read1Buffer, std::string &read2Buffer, const int fileNO);
    void compPEData(const string&r1, const string&r2, const int fileNO);
    int gzcompress(Bytef *data, uLong ndata, Bytef *zdata, uLong *nzdata);
    void compData(const char *buff, const size_t bufSize, const int fileNO);
    void CWoneFastq(const int fileNO);
	bool transBarcodeInfo(string & barcodeInfo);
	void getReadLength(string &fastqFile1, string &fastqFile2);
	

private:
    Barcode m_barcode;
    vector<cmpFile> m_fastq_files;
    vector<cmpFile> m_fastq_files2;
    int b1,l1,b2,l2;
    string m_prefix;  // fq line1 prefix, like @GS10000001L1
    vector<int> m_fastq_size;
    DataInfo* m_info;
    bool m_end;
    int m_barcode_num;
    vector<mutex*> m_fileMutexs;
    vector<mutex*> m_publicBuffMutexs;
    unsigned long long m_maxFastqBuff, m_maxFastqBuff2, m_outFastqBuff, m_outFastqBuff2;
    vector<string> m_fastqBuffs, m_fastqBuffs2;
    FqStat m_fqStat;
    vector< FqstatInfo > m_allFqStatInfo;
    string m_outputDir;
	bool m_isPE;
	vector<int> m_barcode_info;
	int m_read1_length, m_read2_length;
	string m_fastqFile1, m_fastqFile2;
	string slide, lane;
	int maxThreads;
	int fov_buffer_size;
	vector<FovInfo*> m_fovBuffer;
	vector<bool> m_finishFastq;

	// user set parameters
	int m_uThread;
	float m_uMemory;
};

#endif // SPLIT_BARCOED_H
