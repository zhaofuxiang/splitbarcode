//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include <iostream>
#include <string>
using namespace std;

// for thread pool
#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include "SplitBarcode.h"

#include <stdlib.h>
#include <thread>
#include "Timer.h"

void usage()
{
	cout << "USAGE:" << endl;
	cout << "  Version 0.1.6: " << endl;
	cout << "\n  cmd <barcode> <fq> [-OPTION]\n" << endl;
	cout << "OPTION:\n  -2 FILE\t\tFastq for PE mode.[None]" << endl;
	cout << "  -o DIR\t\tOutput dir for decoded fastq.[None]" << endl;
	cout << "  -b INT INT INT\tBarcode information : startCycle, length, mismatchNum.[Last 10 cycle with 1 mismatch]" << endl;
	cout << "  -r\t\t\tApply reverse complement of barcode sequence.[False]" << endl;
	cout << "  -n THREAD\t\tSet the maximum thread numbers.[CPU number]" << endl;
	cout << "  -m MEMORY\t\tSet the maximum memory(GB).[Available memory]" << endl;
	cout << "\n[ERROR] : The parameters number is not correct!" << endl;	
}

char hashIt(const string& s)
{
	char p;
	s.copy(&p, 1, 0);
	return p;
}

void moveParameter(int& argPos, int& argLen)
{
	argPos++;
	if (argPos >= argLen)
	{
		cout << "parameters error, please check" << endl;
		exit(-1);
	}
}



int main(int argc, char **argv)
{
	string barcodeFile = "";
	string fastqFile1 = "";
	string fastqFile2 = "";
	string outDir = "";
	string barcodeInfo = "";
	string reverse = "";
	int uThread = 0;
	float uMemory = 0.0;

	if (argc < 3)
	{
		usage();
		return -1;
	}
	else
	{
		barcodeFile = argv[1];
		fastqFile1 = argv[2];

		fastqFile2 = "";
		outDir = "";
		barcodeInfo = "";
		reverse = "false";


		int Parameter_count = 2;
		while (Parameter_count < argc)
		{
			string para_flag("");
			if (*argv[Parameter_count] == '-' && (*(argv[Parameter_count] + 1) == '-'))
			{
				para_flag = argv[Parameter_count] + 2;
			}
			else if (*argv[Parameter_count] == '-')
			{
				para_flag = argv[Parameter_count] + 1;
			}
			else
			{
				Parameter_count++;
				continue;
			}

			switch (hashIt(para_flag))
			{
			case '2':
				moveParameter(Parameter_count, argc);
				fastqFile2 = argv[Parameter_count];
				break;
			case 'o':
			case 'O':
				moveParameter(Parameter_count, argc);
				outDir = argv[Parameter_count];
				break;
			case 'b':
			case 'B':
				moveParameter(Parameter_count, argc);
				barcodeInfo += argv[Parameter_count];
				barcodeInfo += ',';
				moveParameter(Parameter_count, argc);
				barcodeInfo += argv[Parameter_count];
				barcodeInfo += ',';
				moveParameter(Parameter_count, argc);
				barcodeInfo += argv[Parameter_count];
				barcodeInfo += ',';
				break;
			case 'r':
			case 'R':
				reverse = "true";
				break;
			case 'n':
			case 'N':
				moveParameter(Parameter_count, argc);
				try
				{
					uThread = atoi(argv[Parameter_count]);
					if (uThread < 1)
					{
						cout << "The thread parameter less than 0!" << endl;
						return -1;
					}
				}
				catch (...)
				{
					cout << "The thread parameter error!" << endl;
					return -1;
				}
				break;
			case 'm':
			case 'M':
				moveParameter(Parameter_count, argc);
				try
				{
					uMemory = atof(argv[Parameter_count]);
					if (uMemory < 1)
					{
						cout << "The memory parameter less than 1(GB)!" << endl;
						return -1;
					}
				}
				catch (...)
				{
					cout << "The memory parameter error!" << endl;
					return -1;
				}
				break;
			default:
				cout << "invalid OPTION " << argv[Parameter_count] << endl;
				return -1;
			}
			Parameter_count++;
		}
	}    

	Timer timer;

	if (barcodeInfo[barcodeInfo.size() - 1] == ',')
		barcodeInfo = barcodeInfo.substr(0, barcodeInfo.size() - 1);

    SplitBarcode sb = SplitBarcode();
	sb.setParameters(uThread, uMemory);
	if (!sb.init(fastqFile1, fastqFile2, barcodeFile, barcodeInfo, outDir, reverse))
	{
		cout << "init splitBarcode failed.";
		return -2;
	}

	int thread_num = sb.getThreadNumber();

    thread getDataThread(boost::bind(&SplitBarcode::decodePE, &sb));
	thread readFq1Thread(boost::bind(&SplitBarcode::readFastq, &sb, fastqFile1, 0));
	thread readFq2Thread(boost::bind(&SplitBarcode::readFastq, &sb, fastqFile2, 1));

    boost::asio::io_service ioService;
    boost::thread_group threadpool;
    {
        boost::asio::io_service::work work(ioService);

        for (int i = 0; i < thread_num; ++i)
            threadpool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));

        vector<bool> flags(thread_num, false);
        while (true)
        {
            int status = sb.is_end();
            if (status)
                break;

            if (!status)
            {
                vector<bool> ready_data_index = sb.getReadyDataIndex();

                for (int i = 0; i < ready_data_index.size(); ++i)
                {
                    if (ready_data_index[i] && !flags[i])
                    {
						//cout << "begin a job" << endl;
                        ioService.post(boost::bind(&SplitBarcode::run, &sb, i));
                        flags[i] = true;
                    }
                    else if (!ready_data_index[i])
                        flags[i] = false;

                }
            }
            this_thread::sleep_for(chrono::microseconds(100));
        }
    }

	
	readFq1Thread.join();
	readFq2Thread.join();
	getDataThread.join();

    threadpool.join_all();

    sb.endFastq();

    cout<<"finish write fq total time(s): "<< timer.toc() << endl;
	
    return 0;
}
