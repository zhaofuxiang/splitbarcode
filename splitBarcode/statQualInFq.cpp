//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include "statQualInFq.hpp"
#include <stdint.h>
// NAMESPACE SETTING
using namespace std;

// Calculate estimated error rate by the definition of phred score 
inline double errorProb(const int qScore) {
	return 1.0 / (pow((double)10.0, (double)(qScore*0.1)));
}
inline void InitStructureData(FqstatInfo &fqsInfo) //without name
{
	//init fqsInfo
	fqsInfo.BaseNum = 0;
	fqsInfo.EstErr = 0;
	fqsInfo.GC_Count = 0;
	fqsInfo.N_Count = 0;
	fqsInfo.ReadLen = 0;
	fqsInfo.Q10 = 0;
	fqsInfo.Q20 = 0;
	fqsInfo.Q30 = 0;
	fqsInfo.Q40 = 0;
	fqsInfo.ReadNum = 0;
	memset(&fqsInfo.statMatrix, 0, (2 * MAX_CYCS)*(BASE_TYPE + QUAL_LEN + 10)*sizeof(uint64_t));
}

FqStat::FqStat()
{
	m_read1fileNum = 0;
}

FqStat::~FqStat()
{
	for (int i = 0; i < m_read1fileNum; ++i)
		if (m_StatInfoMutexs[i] != NULL)
		{
			delete m_StatInfoMutexs[i];
			m_StatInfoMutexs[i] = NULL;
		}	
}
void FqStat::InitstatQualInFq(const int fileNum, const int SE_PE_mode) //PE=2,SE=1
{
	int real_filenumber = fileNum*SE_PE_mode;
	m_StatInfoMutexs.resize(real_filenumber,NULL);
	m_FqStatInfo.resize(real_filenumber);
	for (int i = 0; i < real_filenumber; ++i)
		m_StatInfoMutexs[i] = new mutex;
	m_read1fileNum = fileNum;
}
void FqStat::InitFqstatInfo(const std::string &fqName, const int fileNO, const int ReadNO) //ReadNO = 1 means read1, = 2 means read2
{
	int FqStatNO = fileNO;
	if (ReadNO != 1) //not read1
		FqStatNO += m_read1fileNum;
	FqstatInfo &fqsInfo = m_FqStatInfo[FqStatNO];
	fqsInfo.Name = fqName; //fqsInfo.Name.assign(filename);
	InitStructureData(fqsInfo);
}
// MAIN FUNCTION////////////
int FqStat::StatInfo(const char *readBuff, const int fileNO, const int ReadNO) //getStatInfo && synStatInfo, with out of FqstatInfo-struct
{
	FqstatInfo tmpInfo;
	InitStructureData(tmpInfo);
	getStatInfo(readBuff, tmpInfo);
	synStatInfo(tmpInfo,fileNO,ReadNO);
	return 0;
}
int FqStat::getStatInfo(const char *readBuff, FqstatInfo &InfoBuff) //readBuff is not empty
{	/************************************************************************/
	/* get  matric, readLen, ReadNum   --no sum of baseNum                  */
	/************************************************************************/
	uint64_t i = 0, Read_Count=0, R_len=0;

	while( readBuff[i++] != 10 ); //go to the end to the first line
	while( readBuff[i+R_len] != 10 ) 
		R_len++;
	InfoBuff.ReadLen = R_len;
	if (R_len > MAX_CYCS)
	{
		std::cout << "ERROR: ReadLength " << R_len << "  bigger than MAX_CYC " << MAX_CYCS << std::endl;		
	}
	while( true ) 
	{
		Read_Count++;
		const char *basepstr = &readBuff[i];
		for(int j=0; j<R_len; j++) 
		{	
			
			switch(basepstr[j]){				
			case 'A': ++InfoBuff.statMatrix[j][0];
				break;
			case 'C': ++InfoBuff.statMatrix[j][1];
				break;
			case 'G': ++InfoBuff.statMatrix[j][2];
				break;
			case 'T': ++InfoBuff.statMatrix[j][3];
				break;
			case 'N': ++InfoBuff.statMatrix[j][4];
				break;
#ifdef DEBUG_VER
			default : cout<<"error base count "<<j<<" "<<basepstr[j]<<endl;  break;
#endif			
			}
		}
		//InfoBuff.statMatrix[j][QUAL_LEN+BASE_TYPE] //base number of this line

		i += R_len+3; //1 is '\n'
		const char *qualpstr = &readBuff[i];
		for(int j=0; j<R_len; j++) //quality
		{
			int q = qualpstr[j] - MIN_QUAL+5;
#ifdef DEBUG_VER
			if (q < 5 || q>45) // [5, 45]
				cout << "error Q: " << q << endl;
#endif
			++InfoBuff.statMatrix[j][q];			
		}
		i += R_len+1 ; 
		if(readBuff[i] == 0)
			break;
		i += 11; //at least length>11
		while( readBuff[i++] != 10 );
	} 
//#ifdef DEBUG_VER
//	//test InfoBuff.statMatrix
//	testInfo(InfoBuff);	
//#endif
	InfoBuff.ReadNum = Read_Count;
	
	return 0;
}

int FqStat::synAllStatInfo(std::vector<FqstatInfo>& allFqsInfo, int hasBarcode)
{
	int tmp = 0;//no barcode
	if (hasBarcode > 0 && m_FqStatInfo.size() > 2)
	{
		tmp = 1;//0:undecoded;  m_FqStatInfo.size() - 1:ambiguous
	}

	FqstatInfo* pFqstatInfo = &allFqsInfo[0];
	for (int fileNo = tmp; fileNo < m_FqStatInfo.size() - tmp; ++fileNo)
	{
		if (fileNo == m_read1fileNum)
		{
			if (hasBarcode > 0)
			{
				fileNo++;//skip undecoded
			}
			pFqstatInfo = &allFqsInfo[1];
		}

		if (m_FqStatInfo[fileNo].ReadNum == 0)
			continue;

		int Mrow = m_FqStatInfo[fileNo].ReadLen;
		int Mcol = QUAL_LEN + BASE_TYPE + 2;

		if (pFqstatInfo->ReadLen == 0)
			pFqstatInfo->ReadLen = Mrow;


		for (int i = 0; i < Mrow; i++)
		{
			uint64_t *out = pFqstatInfo->statMatrix[i];
			uint64_t *in = m_FqStatInfo[fileNo].statMatrix[i];
			for (int j = 0; j < Mcol; j++)
				out[j] += in[j];
		}
		pFqstatInfo->ReadNum += m_FqStatInfo[fileNo].ReadNum;
	}
	return 0;
}

int FqStat::synAllStatInfo(FqstatInfo &allFqsInfo, int hasBarcode)
{
	int tmp = 0;//no barcode
	if (hasBarcode > 0 && m_FqStatInfo.size() > 2)
	{
		tmp = 1;//0:undecoded;  m_FqStatInfo.size() - 1:ambiguous
	}

	for (int fileNo = tmp; fileNo < m_FqStatInfo.size() - tmp; ++fileNo)
	{
		if (m_FqStatInfo[fileNo].ReadNum == 0)
			continue;

		int Mrow = m_FqStatInfo[fileNo].ReadLen;
		int Mcol = QUAL_LEN + BASE_TYPE + 2;
		if (allFqsInfo.ReadLen == 0)
			allFqsInfo.ReadLen = Mrow;


		for (int i = 0; i < Mrow; i++)
		{
			uint64_t *out = allFqsInfo.statMatrix[i];
			uint64_t *in = m_FqStatInfo[fileNo].statMatrix[i];
			for (int j = 0; j < Mcol; j++)
				out[j] += in[j];
		}
		allFqsInfo.ReadNum += m_FqStatInfo[fileNo].ReadNum;
	}
	return 0;
}

int FqStat::synStatInfo(FqstatInfo &InfoBuff, const int fileNO, const int ReadNO) //ReadNO=1 means read1,=2 means read2; same as SE_PE_mode
{
	int FqStatNO = fileNO;
	if (ReadNO != 1) //not read1
		FqStatNO += m_read1fileNum;
	FqstatInfo &fqsInfo = m_FqStatInfo[FqStatNO];
	//fqsInfo += InfoBuff;
	int Mrow = static_cast<int>(InfoBuff.ReadLen);
	int Mcol = QUAL_LEN + BASE_TYPE + 2;
	if( fqsInfo.ReadLen==0 )		
		fqsInfo.ReadLen = Mrow;
#ifdef DEBUG_VER
	if(fqsInfo.ReadLen != Mrow)
	{
		cout<<"[ERROR] syn statFq info!"<<endl;
		return -1;
	}
#endif		
	
	//lock here
	try{
		//lock_guard<mutex> lck(*m_StatInfoMutexs[FqStatNO]);
		m_StatInfoMutexs[FqStatNO]->lock();
		for (int i = 0; i<Mrow; i++)
		{
			uint64_t *out = fqsInfo.statMatrix[i];
			uint64_t *in = InfoBuff.statMatrix[i];
			for (int j = 0; j<Mcol; j++)
				out[j] += in[j]; //should lock or not???
		}
		fqsInfo.ReadNum += InfoBuff.ReadNum;
		m_StatInfoMutexs[FqStatNO]->unlock();
	}
	catch (std::logic_error&) {
		std::cout <<__LINE__<< "[exception caught] synStatInfo\n";
	}
	return 0;
}

int FqStat::writeStatFile_old( FqstatInfo &fqsInfo ) //one file for a time --- 2016.1.18
{
	FILE *StatFqOutFile;
	
	StatFqOutFile = fopen( (fqsInfo.Name + ".fqStat.txt").c_str(), "wb");
	if( !StatFqOutFile )
	{
		cerr<<"Can't creat statFq file! "<<endl;
		return -1;
	}
	//output
	uint64_t B_num[1000] = {0}; //1000 >> MAX_CYC

	fprintf(StatFqOutFile,"#Name\t%s\n",fqsInfo.Name.c_str());
	fprintf(StatFqOutFile,"#PhredQual\t%d\n",MIN_QUAL);
	fprintf(StatFqOutFile,"#ReadNum\t%llu\n",fqsInfo.ReadNum);

	fprintf(StatFqOutFile,"#row_readLen\t%d\n", static_cast<int>(fqsInfo.ReadLen)); //row for matric
	fprintf(StatFqOutFile,"#col\t%d\n",QUAL_LEN+BASE_TYPE+2); //col for matric	

	fprintf(StatFqOutFile,"#Pos\tA\tC\tG\tT\tN");
	for(int i=0; i<QUAL_LEN; i++)
		fprintf(StatFqOutFile,"\t%d",i);
	fprintf(StatFqOutFile,"\tErr%%\n");  //divide Err

	
	int readLen = static_cast<int>(fqsInfo.ReadLen);
	for(int i=0; i<readLen; i++) //R_NO is the read_Length
	{
		const uint64_t *Qdata = fqsInfo.statMatrix[i];
		double   Qerror = 0.0;

		fqsInfo.N_Count += Qdata[4];
		fqsInfo.GC_Count+= Qdata[1] + Qdata[2];
		fprintf(StatFqOutFile,"%d\t",i);

		for(int j=0; j<BASE_TYPE + QUAL_LEN; j++)
		{
			fprintf(StatFqOutFile,"%llu\t",Qdata[j]);
			if(j<BASE_TYPE) //ACGTN Number
			{
				B_num[i] += Qdata[j]; 				
			}
			else //quality
			{
				//for( int k=0; k< ; k++ )
				if( j>14 ) fqsInfo.Q10 += Qdata[j];
				if( j>24 ) fqsInfo.Q20 += Qdata[j];
				if( j>34 ) fqsInfo.Q30 += Qdata[j];
				if( j>44 ) fqsInfo.Q40 += Qdata[j];
				Qerror += errorProb(j-5)*Qdata[j];
			}
			
		}
		Qerror /= B_num[i]; //B_num[i] the sum of ACGTN in this cycle
		fqsInfo.EstErr += Qerror;
		fprintf( StatFqOutFile,"%.4f\n",Qerror );

		B_num[999] += B_num[i];
	}

	fqsInfo.EstErr /= fqsInfo.ReadLen; //average Err

	fqsInfo.BaseNum = B_num[999];  //total base number

	uint64_t B = B_num[999] / 100; //calculate %

	
	fprintf(StatFqOutFile,"#BaseNum\t%llu\n",fqsInfo.BaseNum);
	fprintf(StatFqOutFile,"#N_Count\t%llu\t%.2f\n",fqsInfo.N_Count, (float)fqsInfo.N_Count/B);
	fprintf(StatFqOutFile,"#GC%%\t%.2f\n", (float)fqsInfo.GC_Count/B);

	fprintf(StatFqOutFile,"#Q10%%\t%.2f\n",(float)fqsInfo.Q10/B);
	fprintf(StatFqOutFile,"#Q20%%\t%.2f\n",(float)fqsInfo.Q20/B);
	fprintf(StatFqOutFile,"#Q30%%\t%.2f\n",(float)fqsInfo.Q30/B);
	//fprintf(StatFqOutFile,"#>Q40%%\t%.2f\n",(float)fqsInfo.Q40/B);

	fprintf(StatFqOutFile,"#EstErr%%\t%f\n",(float)fqsInfo.EstErr); //totle Err

	fclose(StatFqOutFile);
	return 0;
}

int FqStat::writeAllfqStatFile(FqstatInfo &fqsInfo)
{
	FILE *StatFqOutFile;

	std::string statFileName(fqsInfo.Name);
	statFileName += ".fqStat.txt";
	StatFqOutFile = fopen(statFileName.c_str(), "wb");
	if (StatFqOutFile == NULL)
	{
		perror("fopen");
		cerr << "Can't creat statFq file! " << endl;
		return -1;
	}
	//output
	uint64_t B_num[1000] = { 0 }; //1000 >> MAX_CYC

	fprintf(StatFqOutFile, "#Name\tall-fastq-statistic\n");
	fprintf(StatFqOutFile, "#PhredQual\t%d\n", MIN_QUAL);
	fprintf(StatFqOutFile, "#ReadNum\t%llu\n", fqsInfo.ReadNum);

	fprintf(StatFqOutFile, "#row_readLen\t%d\n", fqsInfo.ReadLen); //row for matric
	fprintf(StatFqOutFile, "#col\t%d\n", QUAL_LEN + BASE_TYPE + 2); //col for matric		

	if (fqsInfo.ReadNum == 0) //empty fq
	{
		writeNextEmpty(StatFqOutFile);
		fclose(StatFqOutFile);
		return 0;
	}
	int readLen = fqsInfo.ReadLen;
	for (int i = 0; i < readLen; i++) //R_NO is the read_Length
	{
		//const std::vector<uint64_t> &Qdata = fqsInfo.statMatrix[i]; //TODO:wanghb
		const uint64_t *Qdata = fqsInfo.statMatrix[i];
		double   Qerror = 0.0;

		fqsInfo.N_Count += Qdata[4];
		fqsInfo.GC_Count += Qdata[1] + Qdata[2];
		//fprintf(StatFqOutFile,"%d\t",i+1);

		for (int j = 0; j < BASE_TYPE + QUAL_LEN; j++)
		{
			//fprintf(StatFqOutFile,"%d\t",Qdata[j]);
			if (j<BASE_TYPE) //ACGTN Number
			{
				B_num[i] += Qdata[j];
			}
			else //quality
			{
				//for( int k=0; k< ; k++ )
				if (j>14) fqsInfo.Q10 += Qdata[j];
				if (j > 24) fqsInfo.Q20 += Qdata[j];
				if (j > 34) fqsInfo.Q30 += Qdata[j];
				if (j > 44) fqsInfo.Q40 += Qdata[j];
				Qerror += errorProb(j - 5)*Qdata[j];
			}
		}
		Qerror /= B_num[i]; //B_num[i] the sum of ACGTN in this cycle
		fqsInfo.EstErr += Qerror;
		//fprintf( StatFqOutFile,"%.4f\n",Qerror );

		/*std::cout << "B_num[i]=" << B_num[i] << endl;
		std::cout << "fqsInfo.ReadNum=" << fqsInfo.ReadNum << endl;*/
		if (B_num[i] != fqsInfo.ReadNum)//check statFq
		{
			printf("[statFq error] base number in cycle %d is %llu\n", i, B_num[i]);
			B_num[i] = fqsInfo.ReadNum;
		}
		B_num[999] += B_num[i];
	}
	/*std::cout << "B_num[999]=" << B_num[999]<<endl;
	std::cout << "fqsInfo.ReadLen * fqsInfo.ReadNum=" << fqsInfo.ReadLen * fqsInfo.ReadNum<< endl;*/
	if (B_num[999] != fqsInfo.ReadLen * fqsInfo.ReadNum)//check statFq
	{
		printf("[statFq error] total base number wrong is %llu\n", B_num[999]);
		B_num[999] = fqsInfo.ReadLen * fqsInfo.ReadNum;
	}
	fqsInfo.EstErr /= fqsInfo.ReadLen; //average Err

	fqsInfo.BaseNum = B_num[999];  //total base number
	double B = 1.0*B_num[999] / 100; //calculate %

	fprintf(StatFqOutFile, "#BaseNum\t%llu\n", fqsInfo.BaseNum);
	fprintf(StatFqOutFile, "#N_Count\t%llu\t%.6f\n", fqsInfo.N_Count, (float)fqsInfo.N_Count / B);
	fprintf(StatFqOutFile, "#GC%%\t%.2f\n", (float)fqsInfo.GC_Count / B);

	fprintf(StatFqOutFile, "#Q10%%\t%.2f\n", (float)fqsInfo.Q10 / B);
	fprintf(StatFqOutFile, "#Q20%%\t%.2f\n", (float)fqsInfo.Q20 / B);
	fprintf(StatFqOutFile, "#Q30%%\t%.2f\n", (float)fqsInfo.Q30 / B);
	//fprintf(StatFqOutFile,"#Q40%%\t%.2f\n",(float)fqsInfo.Q40/B);

	fprintf(StatFqOutFile, "#EstErr%%\t%f\n", 100.0*fqsInfo.EstErr); //total Err%
	fprintf(StatFqOutFile, "#Pos\tA\tC\tG\tT\tN");
	for (int i = 0; i < QUAL_LEN + 1; i++)
		fprintf(StatFqOutFile, "\t%d", i);
	fprintf(StatFqOutFile, "\tErr%%\n");  //divide Err
	for (int i = 0; i < readLen; i++)
	{
		fprintf(StatFqOutFile, "%d\t", i + 1);
		//const std::vector<uint64_t> &Qdata = fqsInfo.statMatrix[i];
		const uint64_t *Qdata = fqsInfo.statMatrix[i];
		double   Qerror = 0.0;

		for (int j = 0; j<BASE_TYPE + QUAL_LEN; j++)
		{
			if (j >= BASE_TYPE)
			{
				if (j>14) fqsInfo.Q10 += Qdata[j];
				if (j > 24) fqsInfo.Q20 += Qdata[j];
				if (j > 34) fqsInfo.Q30 += Qdata[j];
				if (j > 44) fqsInfo.Q40 += Qdata[j];
				Qerror += errorProb(j - 5)*Qdata[j];
			}
			fprintf(StatFqOutFile, "%llu\t", Qdata[j]);

		}
		fprintf(StatFqOutFile, "0\t"); //add 0 for 41 = QUAL_LEN
		Qerror /= B_num[i];
		fprintf(StatFqOutFile, "%.4f\n", 100.0*Qerror); //% should *100		
	}
	fclose(StatFqOutFile);
	return 0;
}

const char* FqStat::writeStatFile(const int fileNO, const int ReadNO) //one file for a time
{
	int FqStatNO = fileNO;
	if (ReadNO != 1) //not read1
		FqStatNO += m_read1fileNum;
	FqstatInfo &fqsInfo = m_FqStatInfo[FqStatNO];

	FILE *StatFqOutFile;
	std::string InfoName(fqsInfo.Name);
	InfoName.replace(InfoName.size() - 2, 2, "fqStat.txt");
	StatFqOutFile = fopen( InfoName.c_str(), "wb");
	//strcat( fqsInfo.Name,".fqStat.txt" );
	//StatFqOutFile = fopen( fqsInfo.Name, "wb");
	if( !StatFqOutFile )
	{
		cerr<<"Can't creat statFq file! "<<endl;
		return NULL;
	}
	//output
	uint64_t B_num[1000] = {0}; //1000 >> MAX_CYC

	fprintf(StatFqOutFile,"#Name\t%s\n",fqsInfo.Name.c_str());
	fprintf(StatFqOutFile,"#PhredQual\t%d\n",MIN_QUAL);
	fprintf(StatFqOutFile,"#ReadNum\t%llu\n",fqsInfo.ReadNum);

	fprintf(StatFqOutFile,"#row_readLen\t%d\n", static_cast<int>(fqsInfo.ReadLen)); //row for matric
	fprintf(StatFqOutFile,"#col\t%d\n",QUAL_LEN+BASE_TYPE+2); //col for matric		

	if(fqsInfo.ReadNum == 0) //empty fq
	{
		writeNextEmpty(StatFqOutFile);
		fclose(StatFqOutFile);
		return InfoName.c_str();
	}

	int readLen = static_cast<int>(fqsInfo.ReadLen);
	for(int i=0; i<readLen; i++) //R_NO is the read_Length
	{
		const uint64_t *Qdata = fqsInfo.statMatrix[i];
		double   Qerror = 0.0;

		fqsInfo.N_Count += Qdata[4];
		fqsInfo.GC_Count+= Qdata[1] + Qdata[2];
		//fprintf(StatFqOutFile,"%d\t",i+1);

		for(int j=0; j<BASE_TYPE + QUAL_LEN; j++)
		{
			//fprintf(StatFqOutFile,"%d\t",Qdata[j]);
			if(j<BASE_TYPE) //ACGTN Number
			{
				B_num[i] += Qdata[j]; 				
			}
			else //quality
			{
				//for( int k=0; k< ; k++ )
				if( j>14 ) fqsInfo.Q10 += Qdata[j];
				if( j>24 ) fqsInfo.Q20 += Qdata[j];
				if( j>34 ) fqsInfo.Q30 += Qdata[j];
				if( j>44 ) fqsInfo.Q40 += Qdata[j];
				Qerror += errorProb(j-5)*Qdata[j];
			}
		}
		Qerror /= B_num[i]; //B_num[i] the sum of ACGTN in this cycle
		fqsInfo.EstErr += Qerror;
		//fprintf( StatFqOutFile,"%.4f\n",Qerror );
		if(B_num[i] != fqsInfo.ReadNum )//check statFq
		{
			printf("[statFq error] base number in cycle %d is %llu\n",i,B_num[i]);
			B_num[i] = fqsInfo.ReadNum;
		}
		B_num[999] += B_num[i];
	}
	if(B_num[999] != fqsInfo.ReadLen * fqsInfo.ReadNum)//check statFq
	{
		printf("[statFq error] total base number wrong is %llu\n",B_num[999]);
		B_num[999]=fqsInfo.ReadLen * fqsInfo.ReadNum;
	}
	fqsInfo.EstErr /= fqsInfo.ReadLen; //average Err

	fqsInfo.BaseNum = B_num[999];  //total base number

	double B = 1.0*B_num[999] / 100; //calculate %

	fprintf(StatFqOutFile,"#BaseNum\t%llu\n",fqsInfo.BaseNum);
	fprintf(StatFqOutFile,"#N_Count\t%llu\t%.6f\n",fqsInfo.N_Count, (float)fqsInfo.N_Count/B);
	fprintf(StatFqOutFile,"#GC%%\t%.2f\n", (float)fqsInfo.GC_Count/B);

	fprintf(StatFqOutFile,"#Q10%%\t%.2f\n",(float)fqsInfo.Q10/B);
	fprintf(StatFqOutFile,"#Q20%%\t%.2f\n",(float)fqsInfo.Q20/B);
	fprintf(StatFqOutFile,"#Q30%%\t%.2f\n",(float)fqsInfo.Q30/B);
	//fprintf(StatFqOutFile,"#>Q40%%\t%.2f\n",(float)fqsInfo.Q40/B);

	fprintf(StatFqOutFile,"#EstErr%%\t%f\n",100.0*fqsInfo.EstErr); //total Err%


	fprintf(StatFqOutFile,"#Pos\tA\tC\tG\tT\tN");
	for(int i=0; i<QUAL_LEN+1; i++)
		fprintf(StatFqOutFile,"\t%d",i);
	fprintf(StatFqOutFile,"\tErr%%\n");  //divide Err
	//write matrix in the end 
	for(int i=0; i<readLen; i++) 
	{		
		fprintf(StatFqOutFile,"%d\t",i+1);
		const uint64_t *Qdata = fqsInfo.statMatrix[i];
		double   Qerror = 0.0;

		for(int j=0; j<BASE_TYPE + QUAL_LEN; j++)
		{
			if(j>=BASE_TYPE)
			{
				if( j>14 ) fqsInfo.Q10 += Qdata[j];
				if( j>24 ) fqsInfo.Q20 += Qdata[j];
				if( j>34 ) fqsInfo.Q30 += Qdata[j];
				if( j>44 ) fqsInfo.Q40 += Qdata[j];
				Qerror += errorProb(j-5)*Qdata[j];
			}
			fprintf(StatFqOutFile,"%llu\t",Qdata[j]);

		}
		fprintf(StatFqOutFile,"0\t"); //add 0 for 41 = QUAL_LEN
		Qerror /= B_num[i];
		fprintf( StatFqOutFile,"%.4f\n",100.0*Qerror ); //% should *100		
	}
	fclose(StatFqOutFile);
	return InfoName.c_str();
}

int FqStat::writeNextEmpty( FILE *StatFqOutFile )
{
	fprintf(StatFqOutFile,"#BaseNum\t0\n");
	fprintf(StatFqOutFile,"#N_Count\t0\t0.0\n");
	fprintf(StatFqOutFile,"#GC%%\t0.00\n");

	fprintf(StatFqOutFile,"#Q10%%\t0.00\n");
	fprintf(StatFqOutFile,"#Q20%%\t0.00\n");
	fprintf(StatFqOutFile,"#Q30%%\t0.00\n");
	//fprintf(StatFqOutFile,"#>Q40%%\t0.00\n");

	fprintf(StatFqOutFile,"#EstErr%%\t0.00\n"); //total Err


	fprintf(StatFqOutFile,"#Pos\tA\tC\tG\tT\tN");
	for(int i=0; i<QUAL_LEN; i++)
		fprintf(StatFqOutFile,"\t%d",i);
	fprintf(StatFqOutFile,"\tErr%%\n");  //divide Err

	return 0;
}
