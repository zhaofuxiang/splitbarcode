//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <math.h>
#include <vector>
#include <memory>     //for memset
#include <stdexcept>  //std::logic_error
#include <mutex>
#include <string.h>
using namespace std;

//#if defined(WIN32)
//#include <mutex>
//using namespace std;
//#elif defined(linux)
//#include <boost/thread/mutex.hpp>
////#include <boost/atomic/atomic.hpp> //linux should use boost 
//using namespace boost;
//#endif

#include "MacroDefine.hpp"
// define macro
#define _A 0
#define _C 1
#define _G 2
#define _T 3
#define _N 4
#define MAX_LINE_LENGTH 512
#define MIN_QUAL 33
#define DEFAULT_QUAL 64  
#define BASE_TYPE 5
#define QUAL_LEN 41
#define RESULT_LEN BASE_TYPE+DEFAULT_QUAL+QUAL_LEN-MIN_QUAL
#define GZ_BUFFER_SIZE 100*1024*1024 // Set read buffer to 100Mb

//default EndLine is '\n'  so ENTER_LEN = 1
#define ENTER_LEN 1
typedef unsigned int            uint32_t;  //change that
typedef struct FQ_STATINFO
{	
	uint64_t    ReadLen; 
	uint64_t    ReadNum;   
	//uint64_t ReadNum;
	uint64_t    BaseNum; 
	uint64_t    N_Count;  
	uint64_t    GC_Count; 
	uint64_t    Q10; //Q10 means: the number of bp'qual>=10 --qual
	uint64_t    Q20; 
	uint64_t    Q30; 
	uint64_t    Q40; 
	double      EstErr; 
	std::string      Name;
	//boost::atomic_uint64_t  statMatrix[2 * MAX_CYC][ BASE_TYPE + QUAL_LEN + 10 ]; //1 of 10 is N_Count
	//atomic_int32_t x;  //WIN32 <atomic>
	uint64_t  statMatrix[2 * MAX_CYCS][ BASE_TYPE + QUAL_LEN + 10 ]; //1 of 10 is N_Count
}FqstatInfo;

//declare for the func 
class FqStat
{
public:
	FqStat();
	~FqStat();

	void InitstatQualInFq(const int fileNum, const int SE_PE_mode);
	void InitFqstatInfo(const std::string& fqName, const int fileNO, const int ReadNO);
	/**
	* stat info to public-variaty for every fov===matric, readLen, ReadNum
	*/
	int StatInfo(const char *readBuff, const int fileNO, const int ReadNO);
	/**
		* get stat info for every fov===matric, readLen, ReadNum 
		*/
    int getStatInfo(const char* readBuff, FqstatInfo &InfoBuff); 
		
	/**
		* synthesize each stat info of fov to final stat info maxtix
		*/
    //int synStatInfo(FqstatInfo &InfoBuff, FqstatInfo &fqsInfo); //should lock the resourse
	int synStatInfo(FqstatInfo &InfoBuff, const int fileNO, const int ReadNO);
  
	/**
		* write the final stat info to the file
		*/
	const char* writeStatFile(const int fileNO, const int ReadNO);
	int writeStatFile_old( FqstatInfo &fqsInfo );

	int synAllStatInfo(std::vector<FqstatInfo>& allFqsInfo, int hasBarcode);
	int synAllStatInfo(FqstatInfo &allFqsInfo, int hasBarcode);
	int writeAllfqStatFile(FqstatInfo &fqsInfo);
private:
	int writeNextEmpty( FILE *StatFqOutFile );
	
	std::vector< mutex * > m_StatInfoMutexs; //should be ptr
	std::vector< FqstatInfo > m_FqStatInfo;
	//std::vector< FqstatInfo > m_FqStatInfo2; //read2 add to m_FqStatInfo -- 2016.12.27
	int m_read1fileNum;
	int m_Read1Len ;
	int m_Read2Len ;  
};

