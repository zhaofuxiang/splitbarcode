//-----------------------------------------------------------------------------
// Copyright 2017-2018 (c) BGI.  All Rights Reserved.
// Confidential and proprietary works of BGI.
//-----------------------------------------------------------------------------

/************************************************************************/
/* 
define switch
define para
declare func
*/
/************************************************************************/
#ifndef _MACRO_DEFINE_H_
#define _MACRO_DEFINE_H_

#include <string>


enum WriteFqBarcodePosition
{
	WritefqBarcodePosStart = 1,
	WritefqBarcodePosMiddle,
	WritefqBarcodePosEnd
};

const char BASE[] = "ACGTN";  //BASE[] = "ACGT"
typedef unsigned char uchar;

#define SINGLE_THREAD
#define MAX_COLS     99  //fov max cols
#define MAX_ROWS     99  //fov max rows
#define MAX_CYCS	 1000  //max cycle
#define THREADNUM    10    //default thread number = 10

/**
 * GRR_CYCLE : The cycle used for filter 
 */
#define GRR_CYCLE 25

/************************************************************************/
/* start Quality value                                                  */
/************************************************************************/
#define START_QUAL 33

/**
 * DEBUG_BER : do more check:memory_check and so on; output less info
 * None      : normal version
 */
//#define DEBUG_VER //to test why no fq for a lane

/**
 * BINARY_CALL : Save calls and scores in compressed binary file
 * None        : Save calls and scores in text file
 */
#define BINARY_CALL

#define NEWFILTER
 
#define BIG_CAL
/**
 * COMP_GZ  : compress the fastq file with gz function
 * None     : compress with pigz function
 */
#define COMP_GZ  //use zlib

#define ADD_QUEUE_SIZE 5 
//#ifndef CAL_SIZE
//#define CAL_SIZE   1600500    //for V0.1 block100=908209, v2 1274641
//#endif
//#define UNIT_SIZE  200000000 //200M a data buffer for an element in Queue : 200+(cycle) cal files

// remove the definition of CAL_SIZE
// #define CAL_SIZE GPU_INTENSITY_SIZE

#define MAX_DNB_LEN 7
 //#define MAX_FASTQBUFF 100*1024*1024 //100M*96 < 10G 35cycle=60M/fov
 //#define OUT_FASTQBUFF 5*1024*1024   //5 = 100/20   define in basecallPara.h


//typedef struct DataInfo
//{
//	int cycle;
//	int numBases;
//	int col;
//	int row;
//	unsigned char *Data;
//	std::string Seq;
//	std::string Score;
//}DInfo;



#endif
