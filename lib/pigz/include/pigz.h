﻿#ifndef _PIGZ_H_
#define _PIGZ_H_

#define VERSION "pigz 2.3.1\n"

/* use large file functions if available */
#define _FILE_OFFSET_BITS 64

/* included headers and what is expected from each */
#include <stdio.h>      /* fflush(), fprintf(), fputs(), getchar(), putc(), */
                        /* puts(), printf(), vasprintf(), stderr, EOF, NULL,
                           SEEK_END, size_t, off_t */
#include <stdlib.h>     /* exit(), malloc(), free(), realloc(), atol(), */
                        /* atoi(), getenv() */
#include <stdarg.h>     /* va_start(), va_end(), va_list */
#include <string.h>     /* memset(), memchr(), memcpy(), strcmp(), strcpy() */
                        /* strncpy(), strlen(), strcat(), strrchr() */
#include <errno.h>      /* errno, EEXIST */
#include <assert.h>     /* assert() */
#include <time.h>       /* ctime(), time(), time_t, mktime() */
#include <signal.h>     /* signal(), SIGINT */
#include <sys/types.h>  /* ssize_t */
#ifdef WIN32
#include "wincompat.h"
#else
#include <sys/stat.h>   /* chmod(), stat(), fstat(), lstat(), struct stat, */
                        /* S_IFDIR, S_IFLNK, S_IFMT, S_IFREG */
#include <sys/time.h>   /* utimes(), gettimeofday(), struct timeval */
#include <unistd.h>     /* unlink(), _exit(), read(), write(), close(), */
                        /* lseek(), isatty(), chown() */
#endif
#include <dirent.h>     /* opendir(), readdir(), closedir(), DIR, */
                        /* struct dirent */
#include <fcntl.h>      /* open(), O_CREAT, O_EXCL, O_RDONLY, O_TRUNC, */
                        /* O_WRONLY */
#include <limits.h>     /* PATH_MAX, UINT_MAX, INT_MAX */
#if __STDC_VERSION__-0 >= 199901L || __GNUC__-0 >= 3
#  include <inttypes.h> /* intmax_t */
#endif

#ifdef DEBUG
#  if defined(__APPLE__)
#    include <malloc/malloc.h>
#    define MALLOC_SIZE(p) malloc_size(p)
#  elif defined (__linux)
#    include <malloc.h>
#    define MALLOC_SIZE(p) malloc_usable_size(p)
#  elif defined (_WIN32) || defined(_WIN64)
#    include <malloc.h>
#    define MALLOC_SIZE(p) _msize(p)
#  else
#    define MALLOC_SIZE(p) (0)
#  endif
#endif

#ifdef __hpux
#  include <sys/param.h>
#  include <sys/pstat.h>
#endif

#include "zlib.h"       /* deflateInit2(), deflateReset(), deflate(), */
                        /* deflateEnd(), deflateSetDictionary(), crc32(),
                           inflateBackInit(), inflateBack(), inflateBackEnd(),
                           Z_DEFAULT_COMPRESSION, Z_DEFAULT_STRATEGY,
                           Z_DEFLATED, Z_NO_FLUSH, Z_NULL, Z_OK,
                           Z_SYNC_FLUSH, z_stream */
#if !defined(ZLIB_VERNUM) || ZLIB_VERNUM < 0x1230
#  error Need zlib version 1.2.3 or later
#endif

#ifndef NOTHREAD
#  include "yarn.h"     /* thread, launch(), join(), join_all(), */
                        /* lock, new_lock(), possess(), twist(), wait_for(),
                           release(), peek_lock(), free_lock(), yarn_name */
#endif
#include "deflate.h"     /* ZopfliDeflatePart(), ZopfliInitOptions(),
                                   ZopfliOptions */

/* for local functions and globals */
#define local static

/* prevent end-of-line conversions on MSDOSish operating systems */
#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <io.h>       /* setmode(), O_BINARY */
#  define SET_BINARY_MODE(fd) setmode(fd, O_BINARY)
#else
#  define SET_BINARY_MODE(fd)
#endif

/* release an allocated pointer, if allocated, and mark as unallocated */
#define RELEASE(ptr) \
    do { \
        if ((ptr) != NULL) { \
            FREE(ptr); \
            ptr = NULL; \
        } \
    } while (0)

/* sliding dictionary size for deflate */
#define DICT 32768U

/* largest power of 2 that fits in an unsigned int -- used to limit requests
   to zlib functions that use unsigned int lengths */
#define MAXP2 (UINT_MAX - (UINT_MAX >> 1))

#define RSYNCBITS 12
#define RSYNCMASK ((1U << RSYNCBITS) - 1)
#define RSYNCHIT (RSYNCMASK >> 1)

#define INBUFS(p) (((p)<<1)+3)
#define OUTPOOL(s) ((s)+((s)>>4)+DICT)

/* input buffer size */
#define BUF 32768U

/* globals (modified by main thread only when it's the only thread) */
struct MyStruct {
    char *prog;             /* name by which pigz was invoked */
    int ind;                /* input file descriptor */
    int outd;               /* output file descriptor */
    char inf[PATH_MAX+1];   /* input file name (accommodate recursion) */
    char *outf;             /* output file name (allocated if not NULL) */
    int verbosity;          /* 0 = quiet, 1 = normal, 2 = verbose, 3 = trace */
    int headis;             /* 1 to store name, 2 to store date, 3 both */
    int pipeout;            /* write output to stdout even if file */
    int keep;               /* true to prevent deletion of input file */
    int force;              /* true to overwrite, compress links, cat */
    int form;               /* gzip = 0, zlib = 1, zip = 2 or 3 */
    unsigned char magic1;   /* first byte of possible header when decoding */
    int recurse;            /* true to dive down into directory structure */
    char *sufx;             /* suffix to use (".gz" or user supplied) */
    char *name;             /* name for gzip header */
    time_t mtime;           /* time stamp from input file for gzip header */
    int list;               /* true to list files instead of compress */
    int first;              /* true if we need to print listing header */
    int decode;             /* 0 to compress, 1 to decompress, 2 to test */
    int level;              /* compression level */
    ZopfliOptions zopts;    /* zopfli compression options */
    int rsync;              /* true for rsync blocking */
    int procs;              /* maximum number of compression threads (>= 1) */
    int setdict;            /* true to initialize dictionary in each thread */
    size_t block;           /* uncompressed input size per thread (>= 32K) */
    int warned;             /* true if a warning has been given */

    /* saved gzip/zip header data for decompression, testing, and listing */
    time_t stamp;               /* time stamp from gzip header */
    char *hname;                /* name from header (allocated) */
    unsigned long zip_crc;      /* local header crc */
    unsigned long zip_clen;     /* local header compressed length */
    unsigned long zip_ulen;     /* local header uncompressed length */

    /* globals for decompression and listing buffered reading */
    unsigned char in_buf[BUF];  /* input buffer */
    unsigned char *in_next; /* next unused byte in buffer */
    size_t in_left;         /* number of unused bytes in buffer */
    int in_eof;             /* true if reached end of file on input */
    int in_short;           /* true if last read didn't fill buffer */
    off_t in_tot;           /* total bytes read from input */
    off_t out_tot;          /* total bytes written to output */
    unsigned long out_check;    /* check value of output */

#ifndef NOTHREAD
    /* globals for decompression parallel reading */
    unsigned char in_buf2[BUF]; /* second buffer for parallel reads */
    size_t in_len;          /* data waiting in next buffer */
    int in_which;           /* -1: start, 0: in_buf2, 1: in_buf */
    lock *load_state;       /* value = 0 to wait, 1 to read a buffer */
    thread *load_thread;    /* load_read() thread for joining */
#endif
};

//struct mystruct* is the file_Handle : typedef to pigzFile in nanocall.h

struct MyStruct* pigzOpen(const char *, int, int); //const char* :filename; int:Threads; int:buff_Size to compress

void pigzClose(struct MyStruct*);

void pigzProcess(struct MyStruct*, unsigned char *); //unsigned char * : data format to compress

#endif